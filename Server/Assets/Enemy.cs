﻿using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private NetworkAIController _networkController;

    public float FirePeriod = 1f;
    public List<ActionsStrategyBase> Strategies = new List<ActionsStrategyBase>();
    public int StrategyIndex;

    private DirectionParams _currentDirection;
    private bool _inited;
    private TankController _player;
    private Eagle _eagle;
    public bool IsStuck;
    private PositionsHistory _positionsHistory;

    public Animator ExplodingAnimator;
    public LayerMask ExploderMask;
    public SpriteRenderer View;
    public List<DirectionParams> Directions;
    public DirectionParams LastDirection;
    public float Speed;
    public float BulletSpeed;
    public Bullet CurrentBullet;
    public Bullet BulletPrefab;
    public event EventHandler OnExplosion;
    public GameItemType GameItem;

    public void Init(EnemyModel enemyModel, Vector3 spawnPosition )
    {
        gameObject.transform.position = spawnPosition;
        GameItem = GameItemType.EnemyLevelOne;
        View.sprite = enemyModel.Views.Find(_ => _.Name == "Up").Sprite;
        Speed = enemyModel.Speed;
        BulletSpeed = enemyModel.BulletSpeed;

        foreach (var directionParams in Directions)
        {
            directionParams.Sprite = enemyModel.Views.Find(_ => _.Name == directionParams.DirectionName).Sprite;
        }

        InitCurrentStrategy();
        StartCoroutine(TryFire(FirePeriod));
        DelayChangeStrategy();
        _currentDirection = SelectDirection();

        _positionsHistory = new PositionsHistory(7);
        _positionsHistory.AddPosition(transform.position);
        StartCoroutine(UpdateHistory(0.15f));
        //ConsoleLog.Instance.AddLog($"Create Enemy position: {spawnPosition.x} {spawnPosition.y}");
        
        GameObjectEventChannel.SendEvent(TopicType.EntityToGame, EventType.Initialize, new GamePayload(gameObject, GameItem));
    }

    //private void Start()
    //{
        //GameObjectEventChannel.SendEvent(EventType.Initialize, gameObject, GameItemType.EnemyLevelOne);
    //}

    private IEnumerator UpdateHistory(float period)
    {
        while (true)
        {
            yield return new WaitForSeconds(period);
            _positionsHistory.AddPosition(transform.position);

            if (_positionsHistory.Count() > 5 && _positionsHistory.GetMaxDistance(transform.position) < 0.2f)
            {
                IsStuck = true;
            }
        }
    }

    public void SetPlayerAndEagle(TankController player, Eagle eagle)
    {
        _player = player;
        _eagle = eagle;
    }

    private void InitCurrentStrategy()
    {
        GetCurrentStrategy().Init(_player, _eagle);
    }

    private void DelayChangeStrategy()
    {
        StartCoroutine(ChangeStrategy(GetCurrentStrategy().StrategyDuration));
    }

    private IEnumerator ChangeStrategy(float strategyDuration)
    {
        yield return new WaitForSeconds(strategyDuration);
        StrategyIndex++;
        InitCurrentStrategy();
        DelayChangeStrategy();
    }

    private ActionsStrategyBase GetCurrentStrategy()
    {
        return Strategies[StrategyIndex%Strategies.Count];
    }

    private DirectionParams SelectDirection()
    {
        IsStuck = false;
        var possibleDirections = new List<DirectionParams>(Directions);
        possibleDirections.Remove(_currentDirection);
        return GetCurrentStrategy().SelectDirection(possibleDirections, transform);
    }

    private IEnumerator TryFire(float firePeriod)
    {
        while (true)
        {
            yield return new WaitForSeconds(firePeriod);
            if (_inited)
            {
                CurrentBullet = Fire();
            }
        }
    }

    private Bullet Fire()
    {
        if (CurrentBullet != null)
        {
            return CurrentBullet;
        }
        _networkController.AiFire();

        var bullet = Instantiate(BulletPrefab, transform.parent);
        bullet.Fire(LastDirection, 1, BulletSpeed);
        
        return bullet;
    }

    private void Update()
    {
        if (!_inited)
        {
            var container = GetComponent<NetworkDataContainer>();
            _networkController = container.Data as NetworkAIController; //GetComponent<NetworkAIController>();
            _inited = _networkController != null;
            print("Enemy not inited");
            return;
        }

        if (_networkController.CurrentState == ItemState.Destroy)
        {
            print("Enemy destroyed");
            return;
        }
        
        if (IsStuck)
        {
            _currentDirection = SelectDirection();
        }

        if (_currentDirection != null)
        {
            LastDirection = _currentDirection;
            if (View.sprite != _currentDirection.Sprite)
            {
                View.sprite = _currentDirection.Sprite;
            }
            transform.Translate(_currentDirection.MoveDirection * Speed * Time.deltaTime);
            
            var direction = (DirectionType)Enum.Parse(typeof(DirectionType), LastDirection.DirectionName);
            _networkController.Move(direction);
            
            //GameObjectEventChannel.SendEvent(EventType.Update, gameObject, GameItem);
            GameObjectEventChannel.SendEvent(TopicType.EntityToGame, EventType.Update, new GamePayload(gameObject, GameItem));
        }
    }
    
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (GameUtils.IsInLayerMask(other.gameObject.layer, ExploderMask.value))
        {
            //GameObjectEventChannel.SendEvent(EventType.Destroy, gameObject, GameItem);
            GameObjectEventChannel.SendEvent(TopicType.EntityToGame, EventType.Destroy, new GamePayload(gameObject, GameItem));
            ExplodingAnimator.enabled = true;
            if (OnExplosion != null)
            {
                OnExplosion(this, null);
            }
            Destroy(gameObject);
            Destroy(this);
        }
    }
}