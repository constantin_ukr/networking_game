﻿using System;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Wall : MonoBehaviour
{
    public Tilemap Tilemap;
    public LayerMask ExploderMask;
    //private ParserTilemap _parserTilemap;
    private NetworkDataContainer _parserTilemap;
    
    void Start()
    {
        //_parserTilemap = gameObject.GetComponentInParent<ParserTilemap>();
        _parserTilemap = gameObject.GetComponentInParent<NetworkDataContainer>();
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (!GameUtils.IsInLayerMask(other.gameObject.layer, ExploderMask))
        {
            return;
        }
        
        Vector3 hitPosition = Vector3.zero;
        foreach (var hit in other.contacts)
        {
            hitPosition.x = hit.point.x + 0.01f * hit.normal.x;
            hitPosition.y = hit.point.y + 0.01f * hit.normal.y;
            var cellPosition = Tilemap.WorldToCell(hitPosition);
            Tilemap.SetTile(cellPosition, null);
            
            ((ParserTilemap)_parserTilemap.Data).UpdateData(cellPosition, null);
        }
    }
}