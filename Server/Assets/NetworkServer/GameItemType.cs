﻿public enum GameItemType: byte
{
    None = 0,
    PlayerController = 1,
    Bullet = 2,
    EnemyLevelOne = 3,
    EnemyLevelTwo = 4,
    Egle = 5
}