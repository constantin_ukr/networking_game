﻿using Lidgren.Network;

public class ServerConfig
{
    public string AppIndentifier = "tank_online";
    public string Host;
    public ushort Port = 10000;
    public byte MaxClientCount = 2;

    public NetPeerConfiguration Build()
    {
        NetPeerConfiguration config = new NetPeerConfiguration(AppIndentifier);
        config.Port = Port;
        config.LocalAddress = NetUtility.Resolve(Host);
        config.MaximumConnections = MaxClientCount;
        config.EnableMessageType(NetIncomingMessageType.DiscoveryRequest);
        config.EnableMessageType(NetIncomingMessageType.ConnectionApproval);
        
        return config;
    }
}
