using System.Net;

public class ClientInfo
{
    public string NetProtocol;
    public IPAddress IpAddress;
    public ushort Port;
    public object ConnectionData;
    public string ConnectionId;
}