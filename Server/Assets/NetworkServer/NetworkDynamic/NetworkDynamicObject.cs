﻿using UnityEngine;

public class NetworkDynamicObject : NetworkScript
{
    private bool _wasChangeItemState = false;
    private ItemState _currentStateField = ItemState.None;
    protected byte DirectionTypeValue;

    public Vector3 CurrentPosition;
    
    public GameItemType GameItem = GameItemType.None;

    public NetworkDynamicObject(string name) : base(name)
    {
        
    }
    public ItemState CurrentState
    {
        get { return _currentStateField; }
        set
        {
            if (_currentStateField != value)
            {
                _currentStateField = value;
                /*if (CurrentStateField == ItemState.Update)
                {
                    ConsoleLog.Instance.AddLog(
                        $"id:{NetworkId} state {System.Enum.GetName(typeof(ItemState), CurrentStateField)}");
                }*/

                if (_currentStateField != ItemState.None)
                {
                    _wasChangeItemState = true;
                }
                else
                {
                    _wasChangeItemState = false;
                }
            }
        }
    }
    
    public virtual byte DirectionType
    {
        get { return DirectionTypeValue; }
        set { DirectionTypeValue = value; }
    }

    public void LateUpdate()
    {
        //CurrentPosition = transform.position;
    }

    public virtual bool IsStateChange()
    {
        /*if (_wasChangeItemState)
        {
            ConsoleLog.Instance.AddLog($"id:{NetworkId} state changed");
        }*/
        return _wasChangeItemState;
    }
}
