﻿public enum ItemState : byte
{
    None = 0,
    Initialize = 1,
    Update = 2,
    Destroy = 3
}

