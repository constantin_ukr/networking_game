﻿using Lidgren.Network;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LidgrenNetworkLayer : INetworkLayer
{
    private static NetServer _server;
    private List<ClientInfo> _clientsInfo;
    private System.Threading.Thread m_Thread = null;
    public event ChangeActiveStatusHandler Connect = delegate { };
    public event ChangeActiveStatusHandler Disconnect = delegate { };
    public event ReceiveHandler Receive = delegate { };

    public void Run(object config)
    {
        if (_server != null && (_server.Status == NetPeerStatus.Running || _server.Status == NetPeerStatus.Starting))
        {
            return;
        }
        _clientsInfo = new List<ClientInfo>();
        var cnfg = (NetPeerConfiguration)config;
        _server = new NetServer(cnfg);
        _server.RegisterReceivedCallback(ReceiveData);
        _server.Start();
    }

    public void SendAllExceptSender(Packet packet, ClientInfo except, NetDeliveryMethod method = NetDeliveryMethod.ReliableOrdered)
    {
        var message = _server.CreateMessage();
        message.Write(packet.GetData());
        _server.SendToAll(message, (NetConnection)except.ConnectionData, method, 1);
        packet.Dispose();
    }

    public void SendResponse(Packet packet, ClientInfo receiver, NetDeliveryMethod method = NetDeliveryMethod.ReliableOrdered)
    {
        var message = _server.CreateMessage();
        message.Write(packet.GetData());
        _server.SendMessage(message, (NetConnection)receiver.ConnectionData, method);
        packet.Dispose();
    }

    public void SendToAll(Packet packet, NetDeliveryMethod method = NetDeliveryMethod.ReliableOrdered)
    {
        var message = _server.CreateMessage();
        message.Write(packet.GetData());
        _server.SendToAll(message, method);
        packet.Dispose();
    }

    public void Shutdown(string message)
    {
        _server.Shutdown(message);
    }

    private void ReceiveData(object peer)
    {
        Debug.Log("ReceiveData");
        NetIncomingMessage message = _server.ReadMessage();
        if (message == null)
        {
            return;
        }

        switch (message.MessageType)
        {
            case NetIncomingMessageType.ConnectionApproval:
                Debug.Log("ConnectionApproval");
                string secretKey = message.ReadString();
                if (secretKey == "here have to be the secret key")
                {
                    message.SenderConnection.Approve();
                }
                else
                {
                    message.SenderConnection.Deny();
                }

                break;
            case NetIncomingMessageType.Data:
                Debug.Log("Data");
                Receive(message.Data);
                break;
            case NetIncomingMessageType.StatusChanged:
                Debug.Log("StatusChanged");
                if (message.SenderConnection.Status == NetConnectionStatus.Connected)
                {
                    var clientInfo = new ClientInfo();

                    clientInfo.NetProtocol = "Udp";
                    clientInfo.IpAddress = message.SenderConnection.RemoteEndPoint.Address;
                    clientInfo.Port = (ushort) message.SenderConnection.RemoteEndPoint.Port;
                    clientInfo.ConnectionData = message.SenderConnection;
                    clientInfo.ConnectionId = message.SenderConnection.Peer.UniqueIdentifier.ToString();
                    _clientsInfo.Add(clientInfo);
                    Connect(clientInfo);
                }
                else if (message.SenderConnection.Status == NetConnectionStatus.Disconnected)
                {
                    var client = _clientsInfo.FirstOrDefault(cl =>
                        cl.IpAddress == message.SenderConnection.RemoteEndPoint.Address
                        && cl.Port == message.SenderConnection.RemoteEndPoint.Port
                        && cl.NetProtocol == "Udp");
                    Disconnect(client);
                }

                break;
            case NetIncomingMessageType.DebugMessage:
            case NetIncomingMessageType.WarningMessage:
            case NetIncomingMessageType.ErrorMessage:
                Debug.Log("Debug");
                break;
            default:
                break;
        }

        _server.Recycle(message);
    }
}
