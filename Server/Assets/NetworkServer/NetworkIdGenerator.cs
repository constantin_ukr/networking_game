﻿public static class NetworkIdGenerator
{
    public static short StartFromValue = 1000;
    private static short lastGeneratedValue = StartFromValue;
    
    public static short GenerateNetworkId()
    {
        return lastGeneratedValue++;
    }
}

