﻿using System;
using System.IO;
using UnityEngine;

public class ClientInputHandler : IPacketHandler
{
    public void HandlerPacket(BinaryReader reader, IGameServer gameServer)
    {
        //var type = reader.ReadByte();
        var networkId = reader.ReadInt16();
        var owner = gameServer.GetClientByNetworkId(networkId);
        var direction = reader.ReadByte();
        var x = Convert.ToSingle(reader.ReadInt16() / 1000f);
        var y = Convert.ToSingle(reader.ReadInt16() / 1000f);
        var position = new Vector2(x, y);
        //var pressed = message.ReadByte() > 0;
        var fired = reader.ReadByte() > 0;
        if (owner != null)
        {
            owner.SetReceiveData(position, direction, fired);
        }
    }
}  