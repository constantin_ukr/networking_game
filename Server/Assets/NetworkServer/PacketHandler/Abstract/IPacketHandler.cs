﻿using System.IO;

public interface IPacketHandler
{
    void HandlerPacket(BinaryReader reader, IGameServer gameServer);
}
