public interface IGameServer
{
    NetworkPlayerController GetClientByNetworkId(short networkId);
}
