﻿using System;

public class NetworkController : NetworkDynamicObject
{
    private bool _fired;

    public event EventHandler Fire;
    public event EventHandler ChangeDirection;

    public NetworkController(string name) : base(name)
    {
    }
    
    public bool Fired
    {
        get { return _fired; }
        protected set
        {
            if (_fired != value)
            {
                _fired = value;
                if (_fired && Fire != null)
                {
                    Fire(this, null);
                    _fired = false;
                }
            }
        }
    }

    public override byte DirectionType
    {
        get { return DirectionTypeValue; }
        set
        {
            if (DirectionTypeValue != value)
            {
                if (ChangeDirection != null)
                {
                    DirectionTypeValue = value;
                    ChangeDirection(this, null);
                }
            }
        }
    }
}

