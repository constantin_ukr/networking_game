﻿using UnityEngine;

public class NetworkPlayerController : NetworkController
{
    private ClientInfo _netConnection;

    public ClientInfo Connetion => _netConnection;

	public NetworkPlayerController(string name) : base(name)
	{
	}
	
	public DirectionType GetDirectionType
	{
		get { return (DirectionType) DirectionTypeValue; }
	}

	public void Init(ClientInfo connetion, Vector2 startPosition, byte direction)
	{
		//ConsoleLog.Instance.AddLog($" Init Pos: ({startPosition.x} : {startPosition.y}) MD: {direction}");
		_netConnection = connetion;
		CurrentPosition = startPosition;
		DirectionTypeValue = direction;
		CurrentState = ItemState.Initialize;
	}

	public void SetReceiveData(Vector2 position, byte direction, bool fired)
	{
		CurrentPosition = position;
		DirectionType = direction;
		Fired = fired;
		CurrentState = ItemState.Update;
	}
}
