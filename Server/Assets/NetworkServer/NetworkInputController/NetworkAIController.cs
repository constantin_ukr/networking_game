﻿public class NetworkAIController : NetworkController
{
    private bool _wasChange;

    public NetworkAIController(string name) : base(name)
    {
    }
    
    public  void Start()
    {
        //CurrentPosition = transform.position;
    }

    public void AiFire()
    {
        Fired = true;
        _wasChange = true;
    }

    public void Move(DirectionType direction)
    {
        DirectionTypeValue = (byte)direction;
        _wasChange = true;
    }

    public override bool IsStateChange()
    {
        var result = _wasChange || base.IsStateChange();
        _wasChange = false;
        return result;
    }
}

