﻿using Lidgren.Network;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GameServer2 : MonoBehaviour, ISubscriber, IGameServer
{
    //public delegate void Connected(ClientInfo client);
    //public event Connected OnConnected;
    //public static List<NetworkDataContainer> Clients => _clients;
    public static List<NetworkPlayerController> Clients => _clients;
    
    private static INetworkLayer _server;
    private static List<NetworkPlayerController> _clients;
    private static List<NetworkDynamicObject> _networkObjects;
    private static NetworkScript[] _environment;
    private static ParserTilemap[] _mapData;
    private static PacketHandlerManager _handlerManager;
    
    private void AddClient(NetworkPlayerController client)
    {
        _server.SendResponse(PacketFactory.CreatePacketByType(PacketType.S2C_InitPlayer, client), client.Connetion);
        _clients.Add(client);
        _networkObjects.Add(client);
        //ConsoleLog.Instance.AddLog($"Send => Id: {client.NetworkId} Pos: ({client.CurrentPosition.x} : {client.CurrentPosition.y})");
    }

    private void AddNetworkObject(NetworkDynamicObject networkObject)
    {
        _networkObjects.Add(networkObject);
    }

    private void GameOver(byte result)
    {
        _server.SendToAll(PacketFactory.CreatePacketByType(PacketType.S2C_GameOver, result));
        StartCoroutine(SendBye());
    }

    private IEnumerator SendBye()
    {
        yield return new WaitForSeconds(5);
        
        Shutdown();
        _server = null;
        Application.Quit();
    }

    private void RemoveNetworkObject(short networkId)
    {
        var item = _networkObjects.Find(no => no.NetworkId == networkId);
        if (item != null)
        {
            _networkObjects.Remove(item);
        }
    }

    public void Run()
    {
        _server = new LidgrenNetworkLayer();
        _server.Connect += OnConnect;
        _server.Disconnect += OnDisconnect;
        _server.Receive += OnReceive;
        var config = GetConfig();
        _server.Run(config);

        Debug.Log("Server started");
    }

    private void OnReceive(byte[] message)
    {
        _handlerManager.RunPacketHandler(message, this);
    }

    private void OnConnect(ClientInfo clientInfo)
    {
        GameObjectEventChannel.SendEvent(TopicType.ServerToGame, EventType.Connected, clientInfo);

        _server.SendResponse(PacketFactory.CreatePacketByType(PacketType.S2C_Init_Environment, _environment), clientInfo);
        _server.SendResponse(PacketFactory.CreatePacketByType(PacketType.S2C_Map_Data, _mapData), clientInfo);
        _server.SendResponse(PacketFactory.CreatePacketByType(PacketType.S2C_WorldShapshot, _networkObjects), clientInfo);
    }

    private void OnDisconnect(ClientInfo clientInfo)
    {
        var client = _clients.FirstOrDefault(cl => 
            cl.Connetion.IpAddress == clientInfo.IpAddress && cl.Connetion.Port == clientInfo.Port && cl.Connetion.NetProtocol == clientInfo.NetProtocol);
        if (client == null)
        {
            throw new Exception("Client not found!");
        }        

        // TODO: Validate this place
        //GameObjectEventChannel.SendEvent(EventType.Disconnected, client.gameObject, GameItemType.PlayerController);

        _networkObjects.Remove(client);
        _clients.Remove(client);
        if (_clients.Count > 0)
        {
            _server.SendAllExceptSender(PacketFactory.CreatePacketByType(PacketType.S2C_PeerDisconnected, client.NetworkId), client.Connetion);
        }
    }

    public NetworkPlayerController GetClientByNetworkId(short networkId)
    {
        NetworkPlayerController client = null;
        
        for (int i = 0; i < _clients.Count; i++)
        {
            if (_clients[i].NetworkId == networkId)
            {
                client = _clients[i];
                break;
            }
        }

        return client;
    }

    #region Unity
    void Awake()
    {
        CreateLogConsole();

        _environment = FindObjectsOfType<NetworkDataContainer>().Select(item => item.Data).ToArray();
        _mapData = _environment.OfType<ParserTilemap>().ToArray();

        _clients = new List<NetworkPlayerController>();
        _networkObjects = new List<NetworkDynamicObject>();
        _handlerManager = new PacketHandlerManager();
        _handlerManager.AddHandler(PacketType.C2S_ClientInput, new ClientInputHandler());
        
        GameObjectEventChannel.SubscribeOnEvent(TopicType.GameToServer, EventType.AddClient, this);
        GameObjectEventChannel.SubscribeOnEvent(TopicType.GameToServer, EventType.AddNetworkObject, this);
        GameObjectEventChannel.SubscribeOnEvent(TopicType.GameToServer, EventType.GameOver, this);
    }

    void FixedUpdate()
    {
        if (_clients != null && _clients.Count > 0)
        {
            var data = new List<NetworkDynamicObject>(_networkObjects.Count);
            for (int i = 0; i < _networkObjects.Count; i++)
            {
                if (_networkObjects[i].IsStateChange())
                {
                    //ConsoleLog.Instance.AddLog($"{_networkObjects[i].CurrentState} Id: {_networkObjects[i].NetworkId}");
                    data.Add(_networkObjects[i]);
                }
            }

            data.Capacity = data.Count;
            if (data.Count == 0)
            {
                return;
            }

            var diffMapData = _mapData[0].Diff.Select(item => new Tuple<sbyte, sbyte>((sbyte)item.Item2.TilemapPosition.x, (sbyte)item.Item2.TilemapPosition.y)).ToList();
            _server.SendToAll(PacketFactory.CreatePacketByType(PacketType.S2C_WorldShapshot, data));
            for (var i = 0; i < _networkObjects.Count; i++)
            {
                if (_networkObjects[i].CurrentState == ItemState.Initialize)
                {
                    _networkObjects[i].CurrentState = ItemState.Update;
                }
            }
            if (diffMapData.Count > 0)
            {
                _server.SendToAll(PacketFactory.CreatePacketByType(PacketType.S2C_DiffMapShapshot, diffMapData));
                //ConsoleLog.Instance.AddLog($"Sent_DiffMapShapshot count: {diffMapData.Count}");
                //_mapData[0].SerializeTilemap();
            }
            
            for (int i = 0; i < data.Count; i++)
            {
                if (data[i].CurrentState == ItemState.Destroy)
                {
                    RemoveNetworkObject(data[i].NetworkId);
                    continue;
                }
                data[i].CurrentState = ItemState.None;
            }
        }
    }

    void OnApplicationQuit()
    {
        _server.Connect -= OnConnect;
        _server.Disconnect -= OnDisconnect;
        _server.Receive -= OnReceive;
        Shutdown();
    }

    private void Shutdown()
    {
        if (_server != null)
        {
            _server.Shutdown("bye");
        }
    }

    private void CreateLogConsole()
    {
        var canvas = FindObjectOfType<Canvas>();
        var loggerViewPrefab = Resources.Load("ScrollView") as GameObject;
        var consoleLogInst = loggerViewPrefab.GetComponent<ConsoleLog>();
        if (consoleLogInst == null)
        {
            consoleLogInst = loggerViewPrefab.gameObject.AddComponent<ConsoleLog>();
        }
        var content = loggerViewPrefab.transform.Find("Content");
        var logRowPrefab = Resources.Load("LogRow") as GameObject;
        consoleLogInst.Content = content as RectTransform;
        consoleLogInst.RowTextPrefab = logRowPrefab.transform as RectTransform;
        Instantiate(loggerViewPrefab, canvas.transform);
    }
    #endregion

    private NetPeerConfiguration GetConfig()
    {
        var hostInput = FindObjectsOfType<InputField>().FirstOrDefault(input => input.name == "HostInput");
        var host = hostInput == null || string.IsNullOrWhiteSpace(hostInput.text) ? "127.0.0.1" : hostInput.text; //"192.168.0.19""127.0.0.1"

        var portInput = FindObjectsOfType<InputField>().FirstOrDefault(input => input.name == "PortInput");
        var port = Convert.ToUInt16(portInput == null || string.IsNullOrWhiteSpace(portInput.text) ? "10000" : portInput.text);

        var config = new ServerConfig { Host = host, Port = port }.Build();

        return config;
    }

    public void ReceiveEvent(EventType name, object payload)
    {
        switch (name)
        {
            case EventType.AddClient:
                AddClient((NetworkPlayerController)payload);
                break;
            case EventType.AddNetworkObject:
                AddNetworkObject((NetworkDynamicObject)payload);
                break;
            case EventType.GameOver:
                GameOver((byte)payload);
                break;
            default:
                return;
        }
    }
}
