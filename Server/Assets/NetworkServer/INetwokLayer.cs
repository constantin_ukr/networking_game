﻿using Lidgren.Network;

public delegate void ChangeActiveStatusHandler(ClientInfo clientInfo);
public delegate void ReceiveHandler(byte[] message);


public interface INetworkLayer
{
    void Run(object config);
    event ChangeActiveStatusHandler Connect;
    event ChangeActiveStatusHandler Disconnect;
    event ReceiveHandler Receive;
    void SendResponse(Packet packet, ClientInfo receiver, NetDeliveryMethod method = NetDeliveryMethod.ReliableOrdered);
    void SendAllExceptSender(Packet packet, ClientInfo except, NetDeliveryMethod method = NetDeliveryMethod.ReliableOrdered);
    void SendToAll(Packet packet, NetDeliveryMethod method = NetDeliveryMethod.ReliableOrdered);
    void Shutdown(string message);
}
