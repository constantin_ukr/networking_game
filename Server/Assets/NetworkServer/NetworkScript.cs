﻿public class NetworkScript {

	public short NetworkId;
	public string NameGameObject;

	public NetworkScript(string name)
	{
		NameGameObject = name;
		NetworkId = NetworkIdGenerator.GenerateNetworkId();
	}
}
