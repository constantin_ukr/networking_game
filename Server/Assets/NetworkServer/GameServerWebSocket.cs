﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Lidgren.Network;
using UnityEngine;
using WebSocketSharp;
using WebSocketSharp.Server;
using ErrorEventArgs = WebSocketSharp.ErrorEventArgs;

public class GameServerWebSocket : MonoBehaviour, ISubscriber, IGameServer
{
    //public static List<NetworkPlayerController> Clients => _clients;

    private static WebSocketServer _server;
    private static List<NetworkPlayerController> _clients;
    private static List<NetworkDynamicObject> _networkObjects;
    private static NetworkScript[] _environment;
    private static ParserTilemap[] _mapData;
    private static PacketHandlerManager _handlerManager;
    private static Game _gameService;
    
    void Awake()
    {
        CreateLogConsole();
        
        _environment = FindObjectsOfType<NetworkDataContainer>().Select(item => item.Data).ToArray();
        _mapData = _environment.OfType<ParserTilemap>().ToArray();
        _clients = new List<NetworkPlayerController>();
        _networkObjects = new List<NetworkDynamicObject>();
        _handlerManager = new PacketHandlerManager();
        _handlerManager.AddHandler(PacketType.C2S_ClientInput, new ClientInputHandler());

        GameObjectEventChannel.SubscribeOnEvent(TopicType.GameToServer, EventType.AddClient, this);
        GameObjectEventChannel.SubscribeOnEvent(TopicType.GameToServer, EventType.AddNetworkObject, this);
        GameObjectEventChannel.SubscribeOnEvent(TopicType.GameToServer, EventType.GameOver, this);
    }

    /*void Update()
    {
        if (_server != null)
        {
            //Receive();
        }
    }*/

    //void LateUpdate()
    void FixedUpdate()
    {
        if (_clients != null && _clients.Count > 0)
        {
            var data = new List<NetworkDynamicObject>(_networkObjects.Count);
            for (int i = 0; i < _networkObjects.Count; i++)
            {
                if (_networkObjects[i].IsStateChange())
                {
                    //ConsoleLog.Instance.AddLog($"{_networkObjects[i].CurrentState} Id: {_networkObjects[i].NetworkId} P: {_networkObjects[i].CurrentPosition.x} {_networkObjects[i].CurrentPosition.y}");
                    data.Add(_networkObjects[i]);
                }
            }

            data.Capacity = data.Count;
            if (data.Count == 0)
            {
                return;
            }

            ////var d2 = _mapData[1].Diff;
            SendToAll(PacketFactory.CreatePacketByType(PacketType.S2C_WorldShapshot, data));
            for (var i = 0; i < _networkObjects.Count; i++)
            {
                if (_networkObjects[i].CurrentState == ItemState.Initialize)
                {
                    _networkObjects[i].CurrentState = ItemState.Update;
                }
            }
            var diffMapData = _mapData[0].Diff.Select(item => new Tuple<sbyte, sbyte>((sbyte)item.Item2.TilemapPosition.x, (sbyte)item.Item2.TilemapPosition.y)).ToList();
            if (diffMapData.Count > 0)
            {
                SendToAll(PacketFactory.CreatePacketByType(PacketType.S2C_DiffMapShapshot, diffMapData));
                //ConsoleLog.Instance.AddLog($"Sent_DiffMapShapshot count: {diffMapData.Count}");
                _mapData[0].SerializeTilemap();
            }

            for (int i = 0; i < data.Count; i++)
            {
                if (data[i].CurrentState == ItemState.Destroy)
                {
                    RemoveNetworkObject(data[i].NetworkId);
                    continue;
                }
                data[i].CurrentState = ItemState.None;
            }
        }
    }

    private void CreateLogConsole()
    {
        var canvas = FindObjectOfType<Canvas>();
        var loggerViewPrefab = Resources.Load("ScrollView") as GameObject;
        var consoleLogInst = loggerViewPrefab.GetComponent<ConsoleLog>();
        if (consoleLogInst == null)
        {
            consoleLogInst = loggerViewPrefab.gameObject.AddComponent<ConsoleLog>();
        }
        var content = loggerViewPrefab.transform.Find("Content");
        var logRowPrefab = Resources.Load("LogRow") as GameObject;
        consoleLogInst.Content = content as RectTransform;
        consoleLogInst.RowTextPrefab = logRowPrefab.transform as RectTransform;
        Instantiate(loggerViewPrefab, canvas.transform);
    }

    public void Run()
    {
        if (_server != null)
        {
            return;
        }

        _gameService = new Game();
        _gameService.Connect += OnConnect;
        _gameService.Disconnect += OnDisconnect;
        _gameService.Receive += OnReceive;
        var uri = "ws://127.0.0.1:11001"; // 192.168.0.158 127.0.0.1
        _server = new WebSocketServer(uri);
        _server.Log.Level = LogLevel.Trace;
        
        _server.AddWebSocketService("/Game", () => _gameService);
        _server.Start();

        Debug.Log("Server started");
    }

    private void  OnReceive(byte[] message)
    {
        _handlerManager.RunPacketHandler(message, this);
    }

    private void OnDisconnect(ClientInfo clientInfo)
    {
        var client = _clients.FirstOrDefault(cl => 
            cl.Connetion.IpAddress == clientInfo.IpAddress && cl.Connetion.Port == clientInfo.Port && cl.Connetion.NetProtocol == clientInfo.NetProtocol);
        if (client == null)
        {
            throw new Exception("Client not found!");
        }        

        // TODO: Validate this place
        //GameObjectEventChannel.SendEvent(EventType.Disconnected, client.gameObject, GameItemType.PlayerController);

        _networkObjects.Remove(client);
        _clients.Remove(client);
        if (_clients.Count > 0)
        {
            SendAllExceptSender(PacketFactory.CreatePacketByType(PacketType.S2C_PeerDisconnected, client.NetworkId), client.Connetion);
        }
    }

    private void OnConnect(ClientInfo clientInfo)
    {
        GameObjectEventChannel.SendEvent(TopicType.ServerToGame, EventType.Connected, clientInfo);

        SendResponse(PacketFactory.CreatePacketByType(PacketType.S2C_Init_Environment, _environment), clientInfo);
        SendResponse(PacketFactory.CreatePacketByType(PacketType.S2C_Map_Data, _mapData), clientInfo);
        SendResponse(PacketFactory.CreatePacketByType(PacketType.S2C_WorldShapshot, _networkObjects), clientInfo);
    }

    public NetworkPlayerController GetClientByNetworkId(short networkId)
    {
        NetworkPlayerController client = null;

        for (int i = 0; i < _clients.Count; i++)
        {
            if (_clients[i].NetworkId == networkId)
            {
                client = _clients[i];
                break;
            }
        }

        return client;
    }
    
    private void SendResponse(Packet packet, ClientInfo receiver, NetDeliveryMethod method = NetDeliveryMethod.ReliableOrdered)
    {
        var data = packet.GetData();
        // need to verify
        _gameService.Context.WebSocket.Send(data);
        packet.Dispose();
    }

    private void SendAllExceptSender(Packet packet, ClientInfo except, NetDeliveryMethod method = NetDeliveryMethod.ReliableOrdered)
    {
        var data = packet.GetData();
        foreach (var serviceHost in _server.WebSocketServices.Hosts)
        {
            if (serviceHost.Path == "Game")
            {
                foreach (var session in serviceHost.Sessions.Sessions.Where(state => state.ConnectionState == WebSocketState.Open))
                {
                    if (session.Context.UserEndPoint.Address != except.IpAddress &&
                        session.Context.UserEndPoint.Port != except.Port)
                    {
                        session.Context.WebSocket.Send(data);
                    }
                }
            }
        }

        packet.Dispose();
    }

    private void SendToAll(Packet packet, NetDeliveryMethod method = NetDeliveryMethod.ReliableOrdered)
    {
        var data = packet.GetData();
        var gameHost = _server.WebSocketServices.Hosts.FirstOrDefault(host => host.Path == "/Game");
        if (gameHost != null)
        {
            gameHost.Sessions.Broadcast(data);
        }

        packet.Dispose();
    }
    
    private void RemoveNetworkObject(short networkId)
    {
        var item = _networkObjects.Find(no => no.NetworkId == networkId);
        if (item != null)
        {
            _networkObjects.Remove(item);
            //Destroy(item.gameObject);
            //Destroy(item);
        }
    }

    public void ReceiveEvent(EventType name, object payload)
    {
        switch (name)
        {
            case EventType.AddClient:
                AddClient((NetworkPlayerController)payload);
                break;
            case EventType.AddNetworkObject:
                AddNetworkObject((NetworkDynamicObject)payload);
                break;
            case EventType.GameOver:
                GameOver((byte)payload);
                break;
            default:
                return;
        }
    }

    private void AddClient(NetworkPlayerController client)
    {
        SendResponse(PacketFactory.CreatePacketByType(PacketType.S2C_InitPlayer, client), client.Connetion);
        _clients.Add(client);
        _networkObjects.Add(client);
        //ConsoleLog.Instance.AddLog($"Send => Id: {client.NetworkId} Pos: ({client.CurrentPosition.x} : {client.CurrentPosition.y})");
    }

    private void AddNetworkObject(NetworkDynamicObject networkObject)
    {
        _networkObjects.Add(networkObject);
    }

    private void GameOver(byte result)
    {
        SendToAll(PacketFactory.CreatePacketByType(PacketType.S2C_GameOver, result));
        StartCoroutine(SendBye());
    }

    private IEnumerator SendBye()
    {
        yield return new WaitForSeconds(5);

        _server.Stop();
        //Shutdown();
        _server = null;
        Application.Quit();
    }
}

public class Game : WebSocketBehavior
{
    private List<ClientInfo> _clientsInfo;
    public event ChangeActiveStatusHandler Connect = delegate { };
    public event ChangeActiveStatusHandler Disconnect = delegate { };
    public event ReceiveHandler Receive = delegate { };

    public Game()
    {
        _clientsInfo = new List<ClientInfo>();
    }
    
    protected override void OnClose(CloseEventArgs e)
    {
        Debug.Log("Close");
        Debug.Log(e.Reason);
        Debug.Log(e.Code);
        Debug.Log(e.WasClean);
        Sessions.Broadcast(string.Format("{0} got logged off...", ""));
        var client = _clientsInfo.FirstOrDefault(cl =>
            cl.IpAddress == Context.UserEndPoint.Address
            && cl.Port == Context.UserEndPoint.Port
            && cl.NetProtocol == "WebSocket");
        Disconnect(client);
    }

    protected override void OnError(ErrorEventArgs e)
    {
        Debug.Log("Error");
        Debug.Log(e.Message);
    }

    protected override void OnMessage(MessageEventArgs e)
    {
        Debug.Log("Message");
        if (e.IsBinary)
        {
            Receive(e.RawData);
            
            //Sessions.Broadcast(string.Format("{0}: {1}", _name, Encoding.UTF8.GetString(e.RawData)));
        }
        else if (e.IsText)
        {
            //Sessions.Broadcast(string.Format("{0}: {1}", _name, e.Data));
        }
    }

    protected override void OnOpen()
    {
        Debug.Log("OPEN");
        var clientInfo = new ClientInfo();
                    
        clientInfo.NetProtocol = "WebSocket";
        clientInfo.IpAddress = Context.UserEndPoint.Address;
        clientInfo.Port = (ushort) Context.UserEndPoint.Port;
        clientInfo.ConnectionData = Context;
        // verify session.id for new client
        clientInfo.ConnectionId = Sessions.Count.ToString();
        _clientsInfo.Add(clientInfo);

        Connect(clientInfo);
    }
}
