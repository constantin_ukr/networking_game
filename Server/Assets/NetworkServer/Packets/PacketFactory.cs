﻿using System;
using System.Collections.Generic;
using System.Linq;

public class PacketFactory
{
    public static Packet CreatePacketByType(PacketType packetType, object context)
    {
        switch (packetType)
        {
            case PacketType.S2C_Init_Environment:
            {
                return CreateInitStaticEnvironmentPacket(context);
            }
            case PacketType.S2C_Map_Data:
            {
                return CreateMapPacket(context);
            }
            case PacketType.S2C_InitPlayer:
            {
                return CreateInitPlayerPacket(context);
            }
            case PacketType.S2C_WorldShapshot:
            {
                return CreateWorldSnapshotPacket(context);
            }
            case PacketType.S2C_DiffMapShapshot:
            {
                return CreateDiffMapSnapshotPacket(context);
            }
            case PacketType.S2C_PeerDisconnected:
            {
                return CreatePeerDiconnectedPacket(context);
            }
            case PacketType.S2C_GameOver:
            {
                return CreateGameOverPacket(context);
            }
        }

        throw new Exception("NotSuported type of packet");
    }

    private static Packet CreateInitStaticEnvironmentPacket(object context)
    {
        if (context == null)
        {
            throw new ArgumentNullException("CreateInitStaticEnvironmentPacket");
        }
        var environment = context as NetworkScript[];
        if (environment == null)
        {
            throw new Exception("CreateInitStaticEnvironmentPacket. context as NetworkScript[]");
        }
        
        var data = environment
            .Select(networkScript =>
                new Tuple<short, string>(networkScript.NetworkId, networkScript.NameGameObject))
            .ToArray();

        return new InitStaticEnvironmentPacket(data);
    }

    private static Packet CreateMapPacket(object context)
    {
        if (context == null)
        {
            throw new ArgumentNullException("CreateMapPacket");
        }
        var mapData = context as ParserTilemap[];
        if (mapData == null)
        {
            throw new Exception("CreateMapPacket. context as ParserTilemap[]");
        }
        
        return new MapPacket(mapData);
    }

    private static Packet CreateInitPlayerPacket(object context)
    {
        if (context == null)
        {
            throw new ArgumentNullException("CreateInitPlayerPacket");
        }
        var client = context as NetworkPlayerController;
        if (client == null)
        {
            throw new Exception("CreateInitPlayerPacket. context as NetworkPlayerController");
        }
        
        return new InitPlayerPacket(client.NetworkId, client.CurrentPosition, client.DirectionType);
    }

    private static Packet CreateWorldSnapshotPacket(object context)
    {
        if (context == null)
        {
            throw new ArgumentNullException("CreateWorldSnapshotPacket");
        }
        var data = context as List<NetworkDynamicObject>;
        if (data.Count == 0)
        {
            throw new Exception("CreateWorldSnapshotPacket. context as List<NetworkDynamicObject>");
        }
        
        return new WorldSnapshotPacket(data);
    }
    
    private static Packet CreateDiffMapSnapshotPacket(object context)
    {
        if (context == null)
        {
            throw new ArgumentNullException("CreateDiffMapSnapshotPacket");
        }
        var data = context as List<Tuple<sbyte, sbyte>>;
        if (data.Count == 0)
        {
            throw new Exception("CreateDiffMapSnapshotPacket. context as List<Tuple<sbyte, sbyte>>");
        }
        
        return new DiffMapSnapshotPacket(data);
    }

    private static Packet CreatePeerDiconnectedPacket(object context)
    {
        if (context == null)
        {
            throw new ArgumentNullException("CreatePeerDiconnectedPacket");
        }
        var isCorrectType = context is short;
        if (!isCorrectType)
        {
            throw new Exception("CreatePeerDiconnectedPacket. context is short");
        }
        
        return new PeerDisconnected((short)context);
    }
    
    private static Packet CreateGameOverPacket(object context)
    {
        if (context == null)
        {
            throw new ArgumentNullException("CreateGameOverPacket");
        }
        var isCorrectType = context is byte;
        if (!isCorrectType)
        {
            throw new Exception("CreateGameOverPacket. context is byte");
        }
        
        return new GameOverPacket((byte)context);
    }
}
