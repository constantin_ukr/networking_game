﻿using System;
using UnityEngine;

public class InitPlayerPacket : Packet
{
    public readonly short NetworkId;
    public readonly Vector2 SpawnPosition;
    public readonly DirectionType Direction;

    public InitPlayerPacket(short playerId, Vector2 spawnPosition, byte directionTypeValue)
        : base(PacketType.S2C_InitPlayer)
    {
        NetworkId = playerId;
        SpawnPosition = spawnPosition;
        Direction = (DirectionType) directionTypeValue;

        Buffer.Write(playerId);
        Buffer.Write(Convert.ToInt16(SpawnPosition.x * 1000));
        Buffer.Write(Convert.ToInt16(SpawnPosition.y * 1000));
        Buffer.Write(directionTypeValue);
    }
}