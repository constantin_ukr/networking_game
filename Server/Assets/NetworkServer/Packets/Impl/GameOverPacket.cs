﻿public class GameOverPacket : Packet
{
    public GameOverPacket(byte gameOverResult)
        : base(PacketType.S2C_GameOver)
    {
        Buffer.Write(gameOverResult);
    }
}