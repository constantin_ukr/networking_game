﻿public class MapPacket  : Packet
{
    public MapPacket(ParserTilemap[] maps)
        :base(PacketType.S2C_Map_Data)
    {
        Buffer.Write((byte)maps.Length);
        
        for (int mapIndex = 0; mapIndex < maps.Length; mapIndex++)
        {
            var mapData = maps[mapIndex].MapData;
            Buffer.Write((short)mapData.Count);
            Buffer.Write(maps[mapIndex].NetworkId);
            
            for (int mapDataIndex = 0, len = mapData.Count; mapDataIndex < len; mapDataIndex++)
            {
                var data = mapData[mapDataIndex];
                
                var index = data.SpriteIndex;
                Buffer.Write(index);
                
                var position = data.TilemapPosition;
                Buffer.Write((sbyte)position.x);
                Buffer.Write((sbyte)position.y);
            }            
        }
    }
}