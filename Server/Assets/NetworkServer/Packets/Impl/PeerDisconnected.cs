﻿public class PeerDisconnected : Packet
{
    public PeerDisconnected(short disconnectedPlayerNetworkId)
        : base(PacketType.S2C_PeerDisconnected)
    {
        Buffer.Write(disconnectedPlayerNetworkId);
    }
}