﻿using System;
using System.Collections.Generic;

public class DiffMapSnapshotPacket: Packet
{
    public DiffMapSnapshotPacket(/*NetOutgoingMessage message,*/ List<Tuple<sbyte, sbyte>> mapDiff)
        : base(/*message,*/ PacketType.S2C_DiffMapShapshot)
    {
        Buffer.Write((byte)(mapDiff.Count * 2)); // should add network id and send it for each map
        for (var i = 0; i < mapDiff.Count; i++)
        {
            Buffer.Write(mapDiff[i].Item1);
            Buffer.Write(mapDiff[i].Item2);
        }
        //ConsoleLog.Instance.AddLog($"Send -> DiffMap count: {mapDiff.Count / 2}");
    }
}