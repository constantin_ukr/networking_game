﻿using System;
using System.Text;

public class InitStaticEnvironmentPacket : Packet
{
    public readonly byte EnvironmentNumber;
    public readonly short[] ItemIdes;

    public InitStaticEnvironmentPacket(Tuple<short, string>[] environment)
        :base(PacketType.S2C_Init_Environment)
    {
        EnvironmentNumber = (byte) environment.Length;
        ItemIdes = new short[EnvironmentNumber];
        Buffer.Write((byte)environment.Length);
        for (int i = 0; i < environment.Length; i++)
        {
            ItemIdes[i] = environment[i].Item1;
            var networkId = environment[i].Item1;
            Buffer.Write(networkId);
            var goName = Encoding.UTF8.GetBytes(environment[i].Item2);
            Buffer.Write((byte)goName.Length);
            Buffer.Write(goName);
        }
    }
}
