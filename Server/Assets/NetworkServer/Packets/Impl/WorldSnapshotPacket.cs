﻿using System;
using System.Collections.Generic;

public class WorldSnapshotPacket : Packet
{
    private static int _packetNumber = 0;

    public WorldSnapshotPacket(List<NetworkDynamicObject> networkDynamicObjects)
        : base(PacketType.S2C_WorldShapshot)
    {
        Buffer.Write(_packetNumber);

        var count = (byte)networkDynamicObjects.Count;        
        Buffer.Write(count);
        for (int i = 0, len = count; i < len; i++)
        {
            Buffer.Write(networkDynamicObjects[i].NetworkId);
            Buffer.Write(Convert.ToInt16(networkDynamicObjects[i].CurrentPosition.x * 1000));
            Buffer.Write(Convert.ToInt16(networkDynamicObjects[i].CurrentPosition.y * 1000));
            Buffer.Write(networkDynamicObjects[i].DirectionType);
            var curState = (byte) networkDynamicObjects[i].CurrentState;
            /*if (curState == 2)
            {
                ConsoleLog.Instance.AddLog($"{curState} Id: {networkDynamicObjects[i].NetworkId}");
            }*/
            Buffer.Write(curState);
            Buffer.Write((byte)networkDynamicObjects[i].GameItem);
        }
        ++_packetNumber;
     }
 }
