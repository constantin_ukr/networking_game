﻿using System;
using UnityEngine;

[Serializable]
public struct TileDataNetwork
{
    public sbyte SpriteIndex;
    public Vector2Int TilemapPosition;

    public TileDataNetwork(sbyte spriteIndex, Vector2Int tilemapPosition)
    {
        SpriteIndex = spriteIndex;
        TilemapPosition = tilemapPosition;
    }
}
