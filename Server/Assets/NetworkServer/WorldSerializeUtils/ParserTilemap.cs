﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ParserTilemap : NetworkScript
{
    private readonly Sprite[]_sprites;
    private Tilemap[] _tileMaps;
    public List<TileDataNetwork> MapData;
    public List<Tuple<byte, TileDataNetwork>> Diff;

    public ParserTilemap(string name, Sprite[] sprites, Tilemap[] tileMaps):base(name)
    {
        _sprites = sprites;
        _tileMaps = tileMaps;
        Diff = new List<Tuple<byte, TileDataNetwork>>();
    }
    
    public void SerializeTilemap()
    {
        //Tilemap[] tileMaps = _go.GetComponentsInChildren<Tilemap>();

        var tileDataNetworkInfos = new List<TileDataNetwork>();

        for (int i = 0; i < _tileMaps.Length; i++)
        {
            foreach (var pos in _tileMaps[i].cellBounds.allPositionsWithin)
            {
                Vector3Int localPlace = new Vector3Int(pos.x, pos.y, pos.z);

                if (_tileMaps[i].HasTile(localPlace))
                {
                    var spriteName = _tileMaps[i].GetSprite(localPlace).name;
                    var spriteIndex = (sbyte) Array.FindIndex(_sprites, sprite => sprite.name == spriteName);
                    
                    var tilemapPosition = new Vector2Int(pos.x, pos.y);
                    tileDataNetworkInfos.Add(new TileDataNetwork(spriteIndex, tilemapPosition));
                }
            }
        }

        MapData = tileDataNetworkInfos;
        Diff.Clear();
    }

    public void UpdateData(Vector3Int cellPosition, string spriteName)
    {
        var tilemapPosition = new Vector2Int(cellPosition.x, cellPosition.y);
        var index = (sbyte) Array.FindIndex(_sprites, sprite => sprite.name == spriteName);
        
        if (index == -1)
        {
            MapData.RemoveAll(tm => tm.TilemapPosition.Equals(tilemapPosition));
            Diff.Add(new Tuple<byte, TileDataNetwork>(0, new TileDataNetwork(-1, tilemapPosition)));
        }
        else
        {
            MapData.Add(new TileDataNetwork(index, tilemapPosition));
            Diff.Add(new Tuple<byte, TileDataNetwork>(1, new TileDataNetwork(index, tilemapPosition)));
        }
    }
}


