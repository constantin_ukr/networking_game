﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TankController : MonoBehaviour, ISubscriber
{
	private const GameItemType _gameItem = GameItemType.PlayerController;
    private NetworkPlayerController _networkController;
    private byte _direction;
    private bool _fired;

    public SpriteRenderer View;
	public List<DirectionParams> Directions;
	public DirectionParams LastDirection;
	public float Speed;
	public Bullet CurrentBullet;
	public Bullet BulletPrefab;
	public LayerMask ExploderMask;
	public Animator ExplodingAnimator;
	public float BulletSpeed;

    public void ReceiveEvent(EventType name, object payload)
    {
       /* var entityData = (TankComponentData)payload;
        if (entityData.NetworkId == _currentData.NetworkId)
        {
            _currentData.X = entityData.X;
            _currentData.Y = entityData.Y;
            _currentData.DirectionTypeValue = entityData.DirectionTypeValue;
            _currentData.CurrentStateField = entityData.CurrentStateField;
            _currentData.Fire = entityData.Fire;
        }*/
    }

    private void Awake()
	{
        LastDirection = Directions.FirstOrDefault(_ => _.DirectionName == "Up");
        //_currentData = new TankComponentData(NetworkIdGenerator.GenerateNetworkId(), _gameItem);
        GameObjectEventChannel.SubscribeOnEvent(TopicType.GameToEntity, EventType.Update, this);
    }

	private void Start ()
	{
		_networkController = (NetworkPlayerController) GetComponent<NetworkDataContainer>().Data; //GetComponent<NetworkPlayerController>();
        _networkController.Fire += OnFire;
        _networkController.ChangeDirection += OnChangeDirection;
        _networkController.GameItem = _gameItem;
	}

	private void Update ()
	{
        DirectionParams currentDirection = Directions[_direction];

        if (currentDirection != null)
        {
            LastDirection = currentDirection;
            if (View.sprite != currentDirection.Sprite)
            {
                View.sprite = currentDirection.Sprite;
            }
        }

        transform.position = _networkController.CurrentPosition;

        Fire();

        /*transform.position = new Vector3(_currentData.X, _currentData.Y, 0);        
        DirectionParams currentDirection = Directions[_currentData.DirectionTypeValue]; 

        if (currentDirection != null)
        {
            LastDirection = currentDirection;
            if (View.sprite != currentDirection.Sprite)
            {
                View.sprite = currentDirection.Sprite;
            }
        }

        if(_currentData.Fire)
        {
            Fire();
        }*/
    }

    private void Fire()
	{
		if (CurrentBullet != null)
		{
			return;
		}

        if (_fired)
        {
            var bullet = Instantiate(BulletPrefab, transform.parent);
            CurrentBullet = bullet;
            bullet.Fire(LastDirection, 1, BulletSpeed);
            _fired = false;
        }
	}

	private void OnCollisionEnter2D(Collision2D other)
	{
		if (GameUtils.IsInLayerMask(other.gameObject.layer, ExploderMask.value))
		{
			//GameObjectEventChannel.SendEvent(EventType.Destroy, gameObject, GameItem);
			GameObjectEventChannel.SendEvent(TopicType.EntityToGame, EventType.Destroy, new GamePayload(gameObject, _gameItem));
            GameObjectEventChannel.UnsubscribeOnEvent(TopicType.GameToEntity, EventType.Update, this);

            ExplodingAnimator.enabled = true;
		}
	}

	public void FinishExplosionAnimation()
	{
		//_networkController.Fire -= OnFire;
		Destroy(gameObject);
	}

	private void OnFire(object sender, EventArgs args)
	{
        _fired = true;
	}

	private void OnChangeDirection(object sender, EventArgs args)
	{
        _direction = _networkController.DirectionType;
	}
}
