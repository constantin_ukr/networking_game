﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour, ISubscriber
{
    public List<Transform> PlayerSpawnPositions;
    public TankController TankControllerPrefab;
    public Eagle Eagle;
    public Transform GameRootTransform;
    public EnemyFactory EnemyFactory;

    public LevelModel Level;
    
    private List<TankController> _tankControllers;
    
    private static List<ClientInfo> _connectedClients = new List<ClientInfo>();

    public void ReceiveEvent(EventType eventType, object payload)
    {
        NetworkDataContainer networkObject = null;
        var payloadData =  payload as GamePayload;
        if (payloadData != null)
        {
            if (payloadData.GameItem == GameItemType.Egle && eventType == EventType.Destroy)
            {
                GameObjectEventChannel.SendEvent(TopicType.GameToServer, EventType.GameOver, (byte)0);
                return;
            }

            //var networkObject = gameObject.GetComponent<NetworkDynamicObject>();
            networkObject = payloadData.GameObj.GetComponent<NetworkDataContainer>();
        }

        switch (eventType)
        {            
            case EventType.Initialize:
                if (networkObject == null)
                {
                    if (payloadData.GameItem == GameItemType.Bullet)
                    {
                        //networkObject = gameObject.AddComponent<NetworkDynamicObject>();
                        networkObject = payloadData.GameObj.AddComponent<NetworkDataContainer>();
                        var data = new NetworkDynamicObject(payloadData.GameObj.name);
                        data.CurrentPosition = payloadData.GameObj.transform.position;
                        data.DirectionType = (byte)GetDirectionByZRotation(payloadData.GameObj.GetComponent<Bullet>().View.transform.eulerAngles.z);
                        //networkObject.CurrentPosition = gameObject.transform.position;
                        //networkObject.DirectionType = (byte)GetDirectionByZRotation(gameObject.GetComponent<Bullet>().View.transform.eulerAngles.z);
                        data.CurrentState = ItemState.Initialize;
                        data.GameItem = payloadData.GameItem;
                        networkObject.Data = data;
                    }
                    else if (payloadData.GameItem == GameItemType.EnemyLevelOne || payloadData.GameItem == GameItemType.EnemyLevelTwo)
                    {
                        //networkObject = gameObject.AddComponent<NetworkAIController>();
                        var data = new NetworkAIController(payloadData.GameObj.name);
                        data.CurrentPosition = payloadData.GameObj.transform.position;
                        data.CurrentState = ItemState.Initialize;
                        data.GameItem = payloadData.GameItem;
                        networkObject = payloadData.GameObj.AddComponent<NetworkDataContainer>();
                        networkObject.Data = data;
                    }

                    //networkObject.CurrentState = ItemState.Initialize;
                    //networkObject.GameItem = gameItemType;
                    //_server.AddNetworkObject((NetworkDynamicObject)networkObject.Data);
                    
                    GameObjectEventChannel.SendEvent(TopicType.GameToServer, EventType.AddNetworkObject, networkObject.Data);
                }                
                break;
            case EventType.Update:
                if (networkObject != null)
                {
                    ((NetworkDynamicObject)networkObject.Data).CurrentState = ItemState.Update;
                    ((NetworkDynamicObject)networkObject.Data).CurrentPosition = payloadData.GameObj.transform.position;
                }
                break;
            case EventType.Destroy:
                if (networkObject != null)
                {
                    ((NetworkDynamicObject)networkObject.Data).CurrentState = ItemState.Destroy;
                    // TODO: Hack 
                    if (((NetworkDynamicObject)networkObject.Data).GameItem == GameItemType.PlayerController)
                    {
                        networkObject.gameObject.SetActive(false);
                    }
                }
                break;
            case EventType.Connected:
                OnConnected((ClientInfo)payload);
                break;
            case EventType.Disconnected:
            {
                var disconnectedController = networkObject.GetComponent<TankController>();
                if (_tankControllers.Contains(disconnectedController))
                {
                    _tankControllers.Remove(disconnectedController);
                    var index = _tankControllers.Count - 1 > 0 ? _tankControllers.Count - 1 : 0 ;
                    PlayerSpawnPositions[index].gameObject.SetActive(true);
                    disconnectedController.FinishExplosionAnimation();
                }
                break;
            }
            default:
                return;
        }
    }

    private void Awake()
    {
        _tankControllers = new List<TankController>();
        EnemyFactory.FactoryEmpty += OnEnemyFactoryEmpty;
        GameObjectEventChannel.SubscribeOnEvent(TopicType.EntityToGame, EventType.Initialize, this);
        GameObjectEventChannel.SubscribeOnEvent(TopicType.EntityToGame, EventType.Update, this);
        GameObjectEventChannel.SubscribeOnEvent(TopicType.EntityToGame, EventType.Destroy, this);
        GameObjectEventChannel.SubscribeOnEvent(TopicType.ServerToGame, EventType.Connected, this);
        GameObjectEventChannel.SubscribeOnEvent(TopicType.ServerToGame, EventType.Disconnected, this);
    }

    private void Update()
    {
        if (_connectedClients.Count > 0)
        {
            CreatePlayerController(_connectedClients[0]);
            _connectedClients.RemoveAt(0);
        }
    }

    private void OnEnemyFactoryEmpty() { }

    private void OnConnected(ClientInfo clientConnection)
    {
        _connectedClients.Add(clientConnection);
    }

    private void CreatePlayerController(ClientInfo clientConnection)
    {
        var spawnPosition = GetSpawnPosition();
        var tankController = Instantiate(TankControllerPrefab, GameRootTransform);
        tankController.enabled = false;
        tankController.transform.position = spawnPosition;
        
        var container = tankController.gameObject.AddComponent<NetworkDataContainer>();

        var data = new NetworkPlayerController(gameObject.name);
        container.Data = data;
        var networkData = (NetworkPlayerController)tankController.GetComponent<NetworkDataContainer>().Data;
        var index = Array.FindIndex(tankController.Directions.ToArray(),
            d => d.DirectionName == tankController.LastDirection.DirectionName);
        byte direction = (byte)0;
        if (index == -1)
        {
            print("WTF INDEX");
            direction = (byte)1;
        }
        else
        {
            direction = (byte) index;
        }
        networkData.Init(clientConnection, spawnPosition, direction);
        
        GameObjectEventChannel.SendEvent(TopicType.GameToServer, EventType.AddClient, networkData);
        
        _tankControllers.Add(tankController);
        tankController.enabled = true;

        EnemyFactory.Eagle = Eagle;
        EnemyFactory.TankController = tankController; // WTF but now it is 
        EnemyFactory.Run(Level);
    }
    
    private Vector2 GetSpawnPosition()
    {
        var result = Vector2.zero;
        for (var i = 0; i < PlayerSpawnPositions.Count; i++)
        {
            if (PlayerSpawnPositions[i].gameObject.activeSelf)
            {
                result = PlayerSpawnPositions[i].position;
                PlayerSpawnPositions[i].gameObject.SetActive(false);
                break;
            }
        }

        return result;
    }
    
    private DirectionType GetDirectionByZRotation(float degree)
    {
        if (degree.Equals(270f))
        {
            return DirectionType.Left;
        }
        if (degree.Equals(180f))
        {
            return DirectionType.Up;
        }
        if (degree.Equals(90f))
        {
            return DirectionType.Right;
        }
        return DirectionType.Down;
    }
}
