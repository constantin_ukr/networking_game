﻿using UnityEngine;
using UnityEngine.Tilemaps;

public class MapItems : MonoBehaviour
{
    public Sprite[] Sprites;
    private Tilemap[] _tileMaps;
    
    private void Awake()
    {
        _tileMaps = gameObject.GetComponentsInChildren<Tilemap>();
        var container = gameObject.GetComponent<NetworkDataContainer>();
        container.Data = new ParserTilemap(gameObject.name, Sprites, _tileMaps);
        ((ParserTilemap)container.Data).SerializeTilemap();
    }
}
