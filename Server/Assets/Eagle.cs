﻿using UnityEngine;

public class Eagle : MonoBehaviour
{
    private readonly GameItemType _gameItem = GameItemType.Egle;
    public SpriteRenderer View;
    public LayerMask ExploderMask;
    public Sprite DieView;

    private void Awake()
    {
        var networkData = GetComponent<NetworkDataContainer>();
        if (networkData == null)
        {
            networkData = gameObject.AddComponent<NetworkDataContainer>();
        }
        networkData.Data = new NetworkScript(gameObject.name);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (!GameUtils.IsInLayerMask(other.gameObject.layer, ExploderMask.value))
        {
            return;
        }
        View.sprite = DieView;
        //GameObjectEventChannel.SendEvent(EventType.Destroy, gameObject, GameItem);
        GameObjectEventChannel.SendEvent(TopicType.EntityToGame, EventType.Destroy, new GamePayload(gameObject, _gameItem));
    }
}
