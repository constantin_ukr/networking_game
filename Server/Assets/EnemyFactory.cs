﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DefaultNamespace;
using UnityEngine;
using Random = System.Random;

public class EnemyFactory : MonoBehaviour
{
    public Eagle Eagle;
    public TankController TankController;
    public Enemy EnemyPrefab;
    public List<Enemy> Enemies = new List<Enemy>();
    public LevelModel Level;
    public List<EnemySpawnPositions> SpawnPositions;
    public Transform RootGameObject;
    public Action FactoryEmpty = delegate {};
    private IEnumerator _enemySpawner;
    private bool _run;

    public void Run(LevelModel levelModel)
    {
        if (_run)
        {
            return;
        }

        _run = true;
        //_random = new System.Random();
        Level = levelModel;
       
        _enemySpawner = SpawnEnemy(Level.Enemies, Level.Period);
        StartCoroutine(_enemySpawner);
    }

    public void Stop()
    {
        if (_enemySpawner != null)
        {
            StopCoroutine(_enemySpawner);
        }
    }

    private IEnumerator SpawnEnemy(List<EnemyModel> levelEnemies, float spawnPeriod)
    {
        List<EnemyModel> enemyModels  = new List<EnemyModel>(levelEnemies);
        var i = 0;
        while (enemyModels.Count>0)
        {
            yield return new WaitForSeconds(spawnPeriod);
            var enemyModel = enemyModels[0];
            var enemySpawnPositions = SpawnPositions.Where(_=>_.IsEmpty()).ToList();
            
            if (enemySpawnPositions.Count > 0 && i<5)
            {
                var enemySpawnPosition = enemySpawnPositions[0];
                var enemy = Instantiate(EnemyPrefab, RootGameObject);
                
                enemy.Init(enemyModel, enemySpawnPosition.transform.position);
                enemy.SetPlayerAndEagle(TankController, Eagle);
                enemy.transform.position = enemySpawnPosition.transform.position;
                Enemies.Add(enemy);
                print($"EnemySpawn: {enemy.transform.position}");
                enemy.OnExplosion += (sender, args) => { Enemies.Remove(enemy); --i; };
                enemyModels.Remove(enemyModel);
                ++i;
            }
        }

        FactoryEmpty();
    }
}