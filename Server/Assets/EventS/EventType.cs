﻿public enum EventType: byte
{
    Initialize,
    Update,
    Destroy,
    Connected,
    Disconnected,
    AddClient,
    AddNetworkObject,
    GameOver
}
