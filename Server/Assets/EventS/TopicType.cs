public enum TopicType
{
    EntityToGame,
    GameToEntity,
    GameToServer,
    ServerToGame
}