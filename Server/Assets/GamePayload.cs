using UnityEngine;

public class GamePayload
{
    public readonly GameObject GameObj;
    public readonly GameItemType GameItem;

    public GamePayload(GameObject gameObject, GameItemType gameItem)
    {
        GameObj = gameObject;
        GameItem = gameItem;
    }
}