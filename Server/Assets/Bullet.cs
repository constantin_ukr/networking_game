﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    private readonly GameItemType _gameItem = GameItemType.Bullet;
    public float Speed;
    public Vector2 MoveDirection;
    public SpriteRenderer View;
    public Animator ExplosionAnimation;
    public int Power;
    public bool NeedMove;
    public BoxCollider2D BoxCollider2D;

    public void Fire(DirectionParams direction, int power, float speed)
    {  
        Speed = speed;
        NeedMove = true;
        MoveDirection = direction.MoveDirection;
        View.transform.eulerAngles = new Vector3(0, 0, direction.BulletZRotation);
        transform.position = direction.BulletPosition.position;
        Power = power;
        BoxCollider2D.size = direction.BulletColliderSize;
        //ConsoleLog.Instance.AddLog("sent Instantiate bullet");
        GameObjectEventChannel.SendEvent(TopicType.EntityToGame, EventType.Initialize, new GamePayload(gameObject, _gameItem));
    }

    void Update ()
    {
        if (NeedMove)
        {
            transform.Translate(MoveDirection*Time.deltaTime*Speed);
            gameObject.transform.position = transform.position;
            //ConsoleLog.Instance.AddLog($"sent Update bullet P({transform.position.x * 100}:{transform.position.y * 100})");
            GameObjectEventChannel.SendEvent(TopicType.EntityToGame, EventType.Update, new GamePayload(gameObject, _gameItem));
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        //Debug.Log("OnCollisionEnter2D", other.gameObject);
        ExplosionAnimation.enabled = true;
        NeedMove = false;
        GameObjectEventChannel.SendEvent(TopicType.EntityToGame, EventType.Destroy, new GamePayload(gameObject, _gameItem));
        Destroy(gameObject);
        Destroy(this);
    }
}
