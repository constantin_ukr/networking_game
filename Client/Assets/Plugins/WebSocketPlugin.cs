﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Collections;
using UnityEngine;
using System.Runtime.InteropServices;

public class WebSocketPlugin
{
	private Uri _mUrl;

    public delegate void ChangeState(string state);
    public event ChangeState ChangeStateEvent;

    public WebSocketPlugin(Uri url)
	{
		_mUrl = url;

		string protocol = _mUrl.Scheme;
		if (!protocol.Equals("ws") && !protocol.Equals("wss"))
			throw new ArgumentException("Unsupported protocol: " + protocol);
	}	

	public void SendString(string str)
	{
        var result = Encoding.UTF8.GetBytes(str);

        Send(result);
	}

	public string RecvString()
	{
		byte[] retval = Recv();
        if (retval == null)
        {
            return null;
        }

        var result = Encoding.UTF8.GetString(retval);

        return result;
	}

#if UNITY_WEBGL && !UNITY_EDITOR
	[DllImport("__Internal")]
	private static extern int SocketCreate (string url);

	[DllImport("__Internal")]
	private static extern int SocketState (int socketInstance);

	[DllImport("__Internal")]
	private static extern void SocketSend (int socketInstance, byte[] ptr, int length);

	[DllImport("__Internal")]
	private static extern void SocketRecv (int socketInstance, byte[] ptr, int length);

	[DllImport("__Internal")]
	private static extern int SocketRecvLength (int socketInstance);

	[DllImport("__Internal")]
	private static extern void SocketClose (int socketInstance);

	[DllImport("__Internal")]
	private static extern int SocketError (int socketInstance, byte[] ptr, int length);

	int m_NativeRef = 0;

	public void Send(byte[] buffer)
	{
		SocketSend (m_NativeRef, buffer, buffer.Length);
	}

	public byte[] Recv()
	{
		int length = SocketRecvLength (m_NativeRef);
		if (length == 0)
			return null;
		byte[] buffer = new byte[length];
		SocketRecv (m_NativeRef, buffer, length);
		return buffer;
	}

	public IEnumerator Connect()
	{
		m_NativeRef = SocketCreate (mUrl.ToString());

		while (SocketState(m_NativeRef) == 0)
			yield return 0;
	}
 
	public void Close()
	{
		SocketClose(m_NativeRef);
	}

	public string error
	{
		get {
			const int bufsize = 1024;
			byte[] buffer = new byte[bufsize];
			int result = SocketError (m_NativeRef, buffer, bufsize);

			if (result == 0)
				return null;

			return Encoding.UTF8.GetString (buffer);
		}
	}
#else
    
	WebSocketSharp.WebSocket m_Socket;
	Queue<byte[]> m_Messages = new Queue<byte[]>();
	bool m_IsConnected = false;
	string m_Error = null;

    // version for EchoTest
    /*public IEnumerator Connect()
    {
        m_Socket = new WebSocketSharp.WebSocket(_mUrl.ToString());
        m_Socket.OnMessage += OnMessage;
        m_Socket.OnOpen += OnOpen;
        m_Socket.OnError += OnError;
        m_Socket.OnClose += OnClose;
        m_Socket.ConnectAsync();
        while (!m_IsConnected && m_Error == null)
        {
            yield return 0;
        }
    }*/

    public void Connect(string nickname = null)
	{
		var builder = new UriBuilder(_mUrl);
		//builder.Query = string.Format("name={0}", nickname);
		_mUrl = builder.Uri;
		m_Socket = new WebSocketSharp.WebSocket(_mUrl.ToString());
		m_Socket.OnMessage += OnMessage;
		m_Socket.OnOpen += OnOpen;
		m_Socket.OnError += OnError;
		m_Socket.OnClose += OnClose;
		m_Socket.Connect();
		
		while (m_IsConnected && m_Error == null)
		{
			return;
		}
	}

    public void Send(byte[] buffer)
	{
		if (m_Socket != null && m_Socket.ReadyState == WebSocketSharp.WebSocketState.Open)
		{
			m_Socket.Send(buffer);
		}
	}

	public byte[] Recv()
	{
		if (m_Socket == null || m_Messages.Count == 0)
		{
			return null;
		}

		return m_Messages.Dequeue();
	}

	public void Close()
	{
		if (m_Socket != null)
		{
			m_Socket.Close();
		}
	}

	public string error
	{
		get { return m_Error; }
	}

	private void OnClose(object sender, WebSocketSharp.CloseEventArgs e)
	{
		if (ChangeStateEvent != null)
		{
			ChangeStateEvent("Close");
		}
	}

	private void OnError(object sender, WebSocketSharp.ErrorEventArgs e)
	{
		m_Error = e.Message;
		if (ChangeStateEvent != null)
		{
			ChangeStateEvent("Error");
		}
	}

	private void OnOpen(object sender, EventArgs e)
	{
		m_IsConnected = true;
		if (ChangeStateEvent != null)
		{
			ChangeStateEvent("Open");
		}
	}

	private void OnMessage(object sender, WebSocketSharp.MessageEventArgs e)
	{
		m_Messages.Enqueue(e.RawData);
		if (ChangeStateEvent != null)
		{
			ChangeStateEvent("Message");
		}
	}
#endif 
}