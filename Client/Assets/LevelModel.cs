﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Level", menuName = "BattleCity/Level", order = 1)]
public class LevelModel : ScriptableObject
{
    public List<EnemyModel> Enemies = new List<EnemyModel>();
    public float Period = 3f;
}