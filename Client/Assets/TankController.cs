﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class TankController : MonoBehaviour
{
	public SpriteRenderer View;
	public List<DirectionParams> Directions;
	public DirectionParams LastDirection;
	public float Speed;
	public LayerMask ExploderMask;
	public Animator ExplodingAnimator;
	//public event EventHandler OnExplosion;

	public event EventHandler Fire = delegate { };
    public event EventHandler DirectionAction;

	void Awake()
	{
		LastDirection = Directions.FirstOrDefault(_ => _.DirectionName == "Up");        
	}
	
	private void Update ()
	{
		DirectionParams currentDirection = null;
		
		if (Input.GetButtonDown("Fire1"))
		{
            Fire(this, null);
        }

		foreach (var directionParam in Directions)
		{
			var axisValue = Input.GetAxis(directionParam.Axis.ToString());
			if (Mathf.Abs(axisValue) > Mathf.Abs(directionParam.AxisValue) &&
				Math.Sign(axisValue) == Math.Sign(directionParam.AxisValue))
			{                
				currentDirection = directionParam;
				break;
			}
		}
		
		if (currentDirection != null)
		{
			LastDirection = currentDirection;

		    if (DirectionAction != null)
		    {
		        DirectionAction(this, null);
		    }
			if (View.sprite != currentDirection.Sprite)
			{
				View.sprite = currentDirection.Sprite;
			}
			transform.Translate(LastDirection.MoveDirection * Speed * Time.deltaTime);           
		} 
	}    

	private bool IsInLayerMask(int layer, LayerMask layerMask)
	{
		return layerMask == (layerMask | (1 << layer));
	}

	public void FinishExplosionAnimation()
	{
		Destroy(gameObject);
		/*if (OnExplosion != null)
		{
			OnExplosion(this, null);
		}*/
	}

	private void OnCollisionEnter2D(Collision2D other)
	{
		if (IsInLayerMask(other.gameObject.layer, ExploderMask.value))
		{
			ExplodingAnimator.enabled = true;
		}
	}
}
