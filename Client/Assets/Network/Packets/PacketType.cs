﻿    public enum PacketType: byte
    {
        S2C_Debug = 0,
        S2C_Init_Environment = 1,
        S2C_MapData = 2,
        S2C_InitPlayer = 3,
        S2C_PeerDisconnected = 4,
        C2S_ClientInput = 5,
        
        S2C_WorldSnapshot = 10,
        S2C_DiffMapShapshot = 11,
        S2C_GameOver = 12
    }

