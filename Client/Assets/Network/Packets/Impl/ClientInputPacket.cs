﻿using System;
using UnityEngine;

public class ClientInputPacket : Packet
{
    public readonly short NetworkId;
    public readonly DirectionType MoveDirection;
    public readonly Vector2 Position;
    public readonly bool Fired;

    public ClientInputPacket(NetworkPlayerController controller)
        : base(PacketType.C2S_ClientInput)
    {
        NetworkId = controller.NetworkId;
        Buffer.Write(NetworkId);
        MoveDirection = (DirectionType)controller.MoveDirection;
        Buffer.Write((byte)MoveDirection);
        Position = controller.CurrentPosition;
        Buffer.Write(Convert.ToInt16(Position.x * 1000));
        Buffer.Write(Convert.ToInt16(Position.y * 1000));
        Fired = controller.Fired;
        Buffer.Write(Fired ? (byte)1 : (byte)0);
    }
}
