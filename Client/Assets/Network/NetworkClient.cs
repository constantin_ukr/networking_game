﻿using UnityEngine;
using Lidgren.Network;
using UnityEngine.Tilemaps;
using UnityEngine.UI;
using System.Linq;
using System;

public class NetworkClient : MonoBehaviour, INetworkClient
{
    private Vector2 _lastSendPosition;
    private NetClient _client;
    private NetConnection _connection;
    private bool _isConnected;
    private bool _inited;
    private NetworkScript[] _networkScripts;
    private NetworkPlayerController _player;
    private GameController _game;
    private HandlerManger _handlerManger;

    public NetworkScript[] NetworkScripts => _networkScripts;
    public GameController GameController => _game;
    public NetworkPlayerController Player => _player;
    public bool Inited => _inited;

    private void InitPlayer(object sender, NetworkPlayerController player)
    {
        if (_player == null)
        {
            _player = player;
            _game.PlayerInited -= InitPlayer;
        }
    }
    
    public void Connect()
    {
        if (_connection != null)
        {
            return;
        }

        var hostInput = FindObjectsOfType<InputField>().FirstOrDefault(input => input.name == "HostInput");
        var host = hostInput == null || string.IsNullOrWhiteSpace(hostInput.text) ? "127.0.0.1" : hostInput.text; //"127.0.0.1" "192.168.0.19"

        var portInput = FindObjectsOfType<InputField>().FirstOrDefault(input => input.name == "PortInput");
        var port = Convert.ToUInt16(portInput == null || string.IsNullOrWhiteSpace(portInput.text) ? "10000" : portInput.text);

        NetOutgoingMessage message = _client.CreateMessage("here have to be the secret key");
        _connection = _client.Connect(host, port, message);
        _isConnected = true;
    }

    public void SetInited()
    {
        _inited = true;
    }

    private void Awake()
    {
        ClearTilemap();
        CreateLogConsole();
        _isConnected = false;

        var config = GetConfig();
        _client = new NetClient(config);
        _client.Start();
        _networkScripts = FindObjectsOfType<NetworkScript>();
        _game = FindObjectOfType<GameController>();
        _game.PlayerInited += InitPlayer;

        _handlerManger = new HandlerManger();
        _handlerManger.AddHandler(PacketType.S2C_Init_Environment, new InitEnvironmentHandler());
        _handlerManger.AddHandler(PacketType.S2C_MapData, new MapsHandler());
        _handlerManger.AddHandler(PacketType.S2C_InitPlayer, new InitPlayerHandler());
        _handlerManger.AddHandler(PacketType.S2C_WorldSnapshot, new WorldSnapshotHandler());
        _handlerManger.AddHandler(PacketType.S2C_DiffMapShapshot, new DiffMapSnapshotHandler());
        _handlerManger.AddHandler(PacketType.S2C_PeerDisconnected, new PeerDisconnectedHandler());
        _handlerManger.AddHandler(PacketType.S2C_GameOver, new GameOverHandler());
    }

    void Update()
    {
        if (_isConnected)
        {
            NetIncomingMessage message;
            while ((message = _client.ReadMessage()) != null)
            {
                switch (message.MessageType)
                {
                    case NetIncomingMessageType.Data:
                        ReceiveData(message);
                        break;
                }
            }
        }
    }

    private void CreateLogConsole()
    {
        var canvas = FindObjectOfType<Canvas>();
        var logerViewPrefab = Resources.Load("ScrollView") as GameObject;
        var consoleLogInst = logerViewPrefab.GetComponent<ConsoleLog>();
        if (consoleLogInst == null)
        {
            consoleLogInst = logerViewPrefab.gameObject.AddComponent<ConsoleLog>();
        }
        var content = logerViewPrefab.transform.Find("Content");
        var logRowPrefab = Resources.Load("LogRow") as GameObject;
        consoleLogInst.Content = content as RectTransform;
        consoleLogInst.RowTextPrefab = logRowPrefab.transform as RectTransform;
        Instantiate(logerViewPrefab, canvas.transform);
    }    

    private void LateUpdate()
    {
        if (_isConnected && _player != null && _player.ChangedState)
        {
            SendPacket();
            _player.ChangeStateResume();
        }
    }

    private void OnApplicationQuit()
    {
        _client.Disconnect("bye");
    }

    private NetPeerConfiguration GetConfig()
    {
        var localPort = new System.Random().Next(10001, 10100);

        var config = new NetPeerConfiguration("tank_online");
        config.Port = localPort;
        config.EnableMessageType(NetIncomingMessageType.DiscoveryResponse);

        return config;
    }

    private void ReceiveData(NetIncomingMessage message)
    {
        /*var packetType = (PacketType)message.ReadByte();
        if (packetType == PacketType.S2C_WorldSnapshot)
        {
            //ConsoleLog.Instance.AddLog($"Receive S2C_WorldSnapshot");
        }*/
        _handlerManger.RunHandler(/*packetType,*/ message.Data, this);
    }

    private void SendPacket()
    {        
        var inputPacket = new ClientInputPacket(_player);
        var message = _client.CreateMessage();
        message.Write(inputPacket.GetData());
        _client.SendMessage(message, _connection, NetDeliveryMethod.ReliableOrdered);
        _lastSendPosition = inputPacket.Position;
        //ConsoleLog.Instance.AddLog($"Send Id: {inputPacket.NetworkId} Pos: {inputPacket.Position *  1000}");
        _player.SetLastSendPosition(_lastSendPosition);
    }    
    
    private void ClearTilemap()
    {
        Tilemap[] tilemaps = FindObjectsOfType<Tilemap>();
        
        for (int i = 0; i < tilemaps.Length; i++)
        {
            foreach (var pos in tilemaps[i].cellBounds.allPositionsWithin)
            {
                Vector3Int localPlace = new Vector3Int(pos.x, pos.y, pos.z);

                if (tilemaps[i].HasTile(localPlace))
                {
                    tilemaps[i].SetTile(localPlace, null);
                }
            }
        }
    }
}
