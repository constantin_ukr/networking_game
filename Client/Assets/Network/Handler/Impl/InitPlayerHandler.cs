﻿using System;
using System.IO;

public class InitPlayerHandler : IHandlerPacket
{
    public void HandlerPacket(BinaryReader reader, INetworkClient client)
    {
        var playerId = reader.ReadInt16();
        var x = Convert.ToSingle(reader.ReadInt16() / 1000f);
        var y = Convert.ToSingle(reader.ReadInt16() / 1000f);
        var direction = reader.ReadByte();
        
        var data = new ReceiveData
        {
            EventType = "createPlayer", Data = new GameEntityComponentData
            {
                X = x,
                Y = y,
                NetworkId = playerId,
                Direction = (DirectionType)direction,
                GameItemType = GameItemType.PlayerController,
                CurrentState = ItemState.Initialize
            }
        };
        client.GameController.ReceiveData(data);
    }
}