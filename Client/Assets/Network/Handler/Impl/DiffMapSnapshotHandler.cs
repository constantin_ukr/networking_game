﻿using System;
using System.IO;

public class DiffMapSnapshotHandler : IHandlerPacket
{
    public void HandlerPacket(BinaryReader reader, INetworkClient client)
    {
        var count = reader.ReadByte() / 2;
        var diffMap = new Tuple<sbyte, sbyte>[count];

        for (var i = 0; i < count; i++)
        {
            var x = reader.ReadSByte();
            var y = reader.ReadSByte();
            diffMap[i] = new Tuple<sbyte, sbyte>(x, y);
        }

        client.GameController.ReceiveData(
                new ReceiveData
                {
                    EventType = "diffMap",
                    Data = new DiffMapComponentData { DiffMap = diffMap }
                });
    }
}
