﻿using System;
using System.Collections.Generic;
using System.IO;

public class WorldSnapshotHandler : IHandlerPacket
{
    public void HandlerPacket(BinaryReader reader, INetworkClient client)
    {
        var init = new List<IComponentData>();
        var update = new List<IComponentData>();
        var destroy = new List<IComponentData>();

        var packetNumber = reader.ReadInt32();
        UnityEngine.Debug.Log($"PNumber: {packetNumber}");
        var count = reader.ReadByte();
        for (int i = 0, len = count; i < len; i++)
        {
            var networkId = reader.ReadInt16();
            var x = Convert.ToSingle(reader.ReadInt16() / 1000f);
            var y = Convert.ToSingle(reader.ReadInt16() / 1000f);
            var direction = reader.ReadByte();
            var itemState = (ItemState)reader.ReadByte();
            var gameItem = reader.ReadByte();

            UnityEngine.Debug.Log($"Raw: id {networkId} S: {itemState}");
            if (client != null && client.Player != null)
            {
                if (networkId == client.Player.NetworkId && itemState != ItemState.Destroy)
                {
                    continue;
                }
                // reconnect flow
                if (!client.Inited && itemState == ItemState.Update)
                {
                    //ConsoleLog.Instance.AddLog($"Id {client.Inited} {networkId}");
                    itemState = ItemState.Initialize;
                }

                if(!IsItemExist(client.GameController.NetworkObjects, networkId) &&
                    //!client.GameController.PrevoiusWorldSnapshotInitData.Contains(networkId) &&
                    !client.GameController.PrevoiusWorldSnapshotDeleteData.Contains(networkId) && itemState != ItemState.Destroy)
                    //&& !(networkId == client.Player.NetworkId && itemState == ItemState.Destroy))
                {
                    itemState = ItemState.Initialize;
                }
            }

            var gameEntity = new GameEntityComponentData
            {
                NetworkId = networkId,
                X = x,
                Y = y,
                CurrentState = itemState,
                GameItemType = (GameItemType)gameItem,
                Direction = (DirectionType)direction
            };

            switch (itemState)
            {
                case ItemState.Initialize:
                    //client.GameController.ReceiveData(new ReceiveData { EventType = "createEntity", Data = gameEntity });
                    UnityEngine.Debug.Log($"I: id {gameEntity.NetworkId} P: {gameEntity.X} {gameEntity.Y}");
                    if(client.GameController.PrevoiusWorldSnapshotInitData.Contains(gameEntity.NetworkId))
                        //|| client.GameController.PrevoiusWorldSnapshotDeleteData.Contains(gameEntity.NetworkId))
                    {
                        UnityEngine.Debug.Log($"I: id {gameEntity.NetworkId} IGNORE");
                        continue;
                    }//*/
                    init.Add(gameEntity);
                    client.GameController.PrevoiusWorldSnapshotInitData.Add(gameEntity.NetworkId);
                    break;
                case ItemState.Update:
                    //ConsoleLog.Instance.AddLog($"Update: Id: {dataObject.NetworkId}"); 
                    //client.GameController.ReceiveData(new ReceiveData { EventType = "updateEntity", Data = gameEntity });
                    //UnityEngine.Debug.Log($"U: id {gameEntity.NetworkId} P: {gameEntity.X} {gameEntity.Y}");
                    update.Add(gameEntity);
                    break;
                case ItemState.Destroy:
                    //ConsoleLog.Instance.AddLog($"Destroy: Id: {dataObject.NetworkId}"); 
                    if (!client.Inited)
                    {
                        continue;
                    }
                    //client.GameController.ReceiveData(new ReceiveData { EventType = "deleteEntity", Data = gameEntity });
                    UnityEngine.Debug.Log($"D: id {gameEntity.NetworkId} P: {gameEntity.X} {gameEntity.Y}");
                    destroy.Add(gameEntity);
                    client.GameController.PrevoiusWorldSnapshotDeleteData.Add(gameEntity.NetworkId);
                    break;
                case ItemState.None:
                default:
                    break;
            }
        }
        if (init.Count > 0)
        {            
            client.GameController.ReceiveData(new ReceiveData { EventType = "createEntity", DataArray = init.ToArray() });            
        }
        if (update.Count > 0)
        {
            client.GameController.ReceiveData(new ReceiveData { EventType = "updateEntity", DataArray = update.ToArray() });
        }
        if (destroy.Count > 0)
        {
            client.GameController.ReceiveData(new ReceiveData { EventType = "deleteEntity", DataArray = destroy.ToArray() });
        }
        
        //client.GameController.ReceiveNetworkObjects(init, update, destroy);
        client.SetInited();
    }

    private bool IsItemExist(List<NetworkDynamicObject> data, short itemId)
    {
        for (int i = 0, len = data.Count; i < len; i++)
        {
            if (data[i].NetworkId == itemId)
            {
                return true;
            }
        }

        return false;
    }
}

