﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class MapsHandler : IHandlerPacket
{
    public void HandlerPacket(BinaryReader reader, INetworkClient client)
    {
        var mapLayerCount = reader.ReadByte();
        var tilemapDataList = new List<TileDataNetwork[]>(mapLayerCount);

        for (var mapIndex = 0; mapIndex < tilemapDataList.Capacity; mapIndex++)
        {
            var dataLenght = reader.ReadInt16();
            var tilemapDataInfo = new TileDataNetwork[dataLenght];
            var networkId = reader.ReadInt16();

            var parser = client.NetworkScripts.FirstOrDefault(ngo => ngo.NetworkId == networkId) as ParserTilemap;
            for (var i = 0; i < tilemapDataInfo.Length; i++)
            {
                var indexSprite = reader.ReadSByte();
                var pos = new Vector2Int(reader.ReadSByte(), reader.ReadSByte());
                tilemapDataInfo[i] = new TileDataNetwork { SpriteIndex = indexSprite, TilemapPosition = pos };
            }
            tilemapDataList.Add(tilemapDataInfo);
            parser.DeserializeTilemap(tilemapDataInfo);
        }
    }
}
