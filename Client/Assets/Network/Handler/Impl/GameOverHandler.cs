﻿using System.IO;

public class GameOverHandler : IHandlerPacket
{
    public void HandlerPacket(BinaryReader reader, INetworkClient client)
    {
        var result = reader.ReadByte();
        client.GameController.ReceiveData(new ReceiveData { EventType = "gameOver", Data = new GameOverComponentData { Result = result } });
    }
}