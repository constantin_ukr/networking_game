﻿using System;
using System.IO;
using System.Linq;
using System.Text;

public class InitEnvironmentHandler : IHandlerPacket
{
    public void HandlerPacket(BinaryReader reader, INetworkClient client)
    {
        var networkItemCount = reader.ReadByte();
        for (var i = 0; i < networkItemCount; i++)
        {
            var networkId = reader.ReadInt16();
            var itemNameLength = reader.ReadByte();
            var name = Encoding.UTF8.GetString(reader.ReadBytes(itemNameLength));
            var go = client.NetworkScripts.FirstOrDefault(ngo => ngo.NetworkGameObjectName == name);
            if (go == null)
            {
                throw new Exception("GO not found" + name);
            }
            go.NetworkId = networkId;
        }
    }
}
