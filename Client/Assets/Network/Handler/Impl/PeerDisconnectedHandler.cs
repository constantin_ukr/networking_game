﻿using System.IO;

public class PeerDisconnectedHandler : IHandlerPacket
{
    public void HandlerPacket(BinaryReader reader, INetworkClient client)
    {
        var disconnectedPeerWithNetworkId = reader.ReadInt16();
        client.GameController.PlayerDisconnected(disconnectedPeerWithNetworkId);
    }
}