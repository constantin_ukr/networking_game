﻿using System.IO;

public interface IHandlerPacket
{
    void HandlerPacket(BinaryReader reader, INetworkClient client);
}

