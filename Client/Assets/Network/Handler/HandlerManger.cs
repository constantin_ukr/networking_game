﻿using System.Collections.Generic;
using System.IO;

public class HandlerManger
{
    private Dictionary<PacketType, IHandlerPacket> _handlers = new Dictionary<PacketType, IHandlerPacket>();

    public void AddHandler(PacketType packetType, IHandlerPacket handler)
    {
        if(!_handlers.ContainsKey(packetType))
        {
            _handlers.Add(packetType, handler);
        }
    }

    public void RunHandler(byte[] message, INetworkClient client)
    {
        using (var stream = new MemoryStream(message))
        {
            using (var reader = new BinaryReader(stream))
            {
                var packetType = (PacketType) reader.ReadByte();
                //UnityEngine.Debug.Log($"Handle of type {packetType}");
                if (_handlers.ContainsKey(packetType))
                {
                    _handlers[packetType].HandlerPacket(reader, client);
                }
            }
        }
    }
}
