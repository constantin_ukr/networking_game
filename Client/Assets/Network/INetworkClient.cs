﻿public interface INetworkClient
{
    NetworkScript[] NetworkScripts { get; }
    GameController GameController { get; }
    NetworkPlayerController Player { get; }
    bool Inited { get; }

    //void InitPlayer(NetworkPlayerController player);
    void SetInited();
}

