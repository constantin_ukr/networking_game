﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

public class NetworkClientWebSocket : MonoBehaviour, INetworkClient
{
    private Vector2 _lastSendPosition;
    private WebSocketPlugin _client;
    private bool _isConnected;
    private bool _inited;
    private NetworkScript[] _networkScripts;
    private NetworkPlayerController _player;
    private GameController _game;
    private HandlerManger _handlerManger;

    public NetworkScript[] NetworkScripts => _networkScripts;
    public GameController GameController => _game;
    public NetworkPlayerController Player => _player;
    public bool Inited => _inited;

    private void InitPlayer(object sender, NetworkPlayerController player)
    {
        if (_player == null)
        {
            _player = player;
            _game.PlayerInited -= InitPlayer;
        }
    }

    public void Connect()
    {
        var hostInput = FindObjectsOfType<InputField>().FirstOrDefault(input => input.name == "HostInput");
        var host = hostInput == null || string.IsNullOrWhiteSpace(hostInput.text) ? "127.0.0.1" : hostInput.text; //"127.0.0.1" "192.168.0.19" 192.168.0.158

        var portInput = FindObjectsOfType<InputField>().FirstOrDefault(input => input.name == "PortInput");
        var port = Convert.ToUInt16(portInput == null || string.IsNullOrWhiteSpace(portInput.text) ? "11001" : portInput.text);

        var uri = new Uri(string.Format("ws://{0}:{1}/Game", host, port));
        _client = new WebSocketPlugin(uri);
        _client.ChangeStateEvent += OnChangeState;
        _client.Connect();
    }

    public void SetInited()
    {
        _inited = true;
    }

    private void Awake()
    {
        ClearTilemap();
        CreateLogConsole();
        _isConnected = false;

        _networkScripts = FindObjectsOfType<NetworkScript>();
        _game = FindObjectOfType<GameController>();
        _game.PlayerInited += InitPlayer;

        _handlerManger = new HandlerManger();
        _handlerManger.AddHandler(PacketType.S2C_Init_Environment, new InitEnvironmentHandler());
        _handlerManger.AddHandler(PacketType.S2C_MapData, new MapsHandler());
        _handlerManger.AddHandler(PacketType.S2C_InitPlayer, new InitPlayerHandler());
        _handlerManger.AddHandler(PacketType.S2C_WorldSnapshot, new WorldSnapshotHandler());
        _handlerManger.AddHandler(PacketType.S2C_DiffMapShapshot, new DiffMapSnapshotHandler());
        _handlerManger.AddHandler(PacketType.S2C_PeerDisconnected, new PeerDisconnectedHandler());
        _handlerManger.AddHandler(PacketType.S2C_GameOver, new GameOverHandler());
    }

    private void CreateLogConsole()
    {
        var canvas = FindObjectOfType<Canvas>();
        var logerViewPrefab = Resources.Load("ScrollView") as GameObject;
        var consoleLogInst = logerViewPrefab.GetComponent<ConsoleLog>();
        if (consoleLogInst == null)
        {
            consoleLogInst = logerViewPrefab.gameObject.AddComponent<ConsoleLog>();
        }
        var content = logerViewPrefab.transform.Find("Content");
        var logRowPrefab = Resources.Load("LogRow") as GameObject;
        consoleLogInst.Content = content as RectTransform;
        consoleLogInst.RowTextPrefab = logRowPrefab.transform as RectTransform;
        Instantiate(logerViewPrefab, canvas.transform);
    }

    private void FixedUpdate()
    {
        if (_isConnected)
        {
            if (_player != null && _player.ChangedState)
            {
                SendPacket();
                _player.ChangeStateResume();
            }
        }
    }

    private void OnApplicationQuit()
    {
        _client.Close();
    }

    private void OnChangeState(string eventType)
    {
        //ConsoleLog.Instance.AddLog($"OnChangeState {eventType}");
        if (eventType == "Message")
        {
            //print(eventType);
            var message = _client.Recv();
            if (message == null)
            {
                //print("Recive empty array");
                return;
            }
            // the second param must be verify. how this catch into the method down
            _handlerManger.RunHandler(message, this);
        }
        else if (eventType == "Open")
        {
            _isConnected = true;
            //print(eventType);                
        }
        else if (eventType == "Close")
        {
            print(eventType);
            //_game.
        }
        else if (eventType == "Error")
        {
            //print(_client.error);
            //print(eventType);
        }
    }

    private void SendPacket()
    {
        var inputPacket = new ClientInputPacket(_player);
        _client.Send(inputPacket.GetData());
        _lastSendPosition = inputPacket.Position;
        //ConsoleLog.Instance.AddLog($"Send Id: {inputPacket.NetworkId} Pos: {inputPacket.Position *  1000}");
        _player.SetLastSendPosition(_lastSendPosition);
    }

    private void ClearTilemap()
    {
        Tilemap[] tilemaps = FindObjectsOfType<Tilemap>();

        for (int i = 0; i < tilemaps.Length; i++)
        {
            foreach (var pos in tilemaps[i].cellBounds.allPositionsWithin)
            {
                Vector3Int localPlace = new Vector3Int(pos.x, pos.y, pos.z);

                if (tilemaps[i].HasTile(localPlace))
                {
                    tilemaps[i].SetTile(localPlace, null);
                }
            }
        }
    }
}
