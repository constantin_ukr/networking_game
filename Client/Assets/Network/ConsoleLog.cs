﻿using UnityEngine;
using UnityEngine.UI;

public class ConsoleLog : MonoBehaviour
{
    public static ConsoleLog Instance = null;

    public RectTransform RowTextPrefab;
    public RectTransform Content;

    public void AddLog(string log)
    {
        var instance = Instantiate(RowTextPrefab.gameObject) as GameObject;
        instance.transform.SetParent(Content, false);
        var text = instance.GetComponent<Text>();
        text.text = log;
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }
}
