﻿using UnityEngine;

[System.Serializable]
public struct TileDataNetwork
{
    public sbyte SpriteIndex;
    public Vector2Int TilemapPosition;
}