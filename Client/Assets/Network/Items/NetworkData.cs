﻿using System;
using UnityEngine;

[Serializable]
public class NetworkData
{
    public short NetworkId;
    public byte MoveDirection;
    public Vector2 CurrentPosition;
    public ItemState CurrentState = ItemState.None;
    public GameItemType GameItemType = GameItemType.None;
}
