﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ParserTilemap : NetworkScript
{
    public Sprite[] sprites;
    private Tilemap _map;
    private List<TileDataNetwork[]> _data = new List<TileDataNetwork[]>(); 
        
    protected override void Awake()
    {
        //_data = new List<TileDataNetwork[]>(2);
        base.Awake();
        _map = GetComponentInChildren<Tilemap>();
        if (_map == null)
        {
            throw new Exception("Tilemap not found");
        }
    }

    private void Update()
    {
        if (_data.Count > 0)
        {
            while (true)
            {
                var tilemapData = _data[0];
                for (var i = 0; i < tilemapData.Length; i++)
                {
                    var tileData = ScriptableObject.CreateInstance<Tile>();
                    tileData.sprite = sprites[tilemapData[i].SpriteIndex];
                    var posFull = new Vector3Int(tilemapData[i].TilemapPosition.x, tilemapData[i].TilemapPosition.y, 0);
                    _map.SetTile(posFull, tileData);
                }
                _data.RemoveAt(0);
                if (_data.Count == 0)
                {
                    break;
                }
            }
        }
    }

    public void DeserializeTilemap(TileDataNetwork[] tilemapData)
    {
        _data.Add(tilemapData);
    }
}


