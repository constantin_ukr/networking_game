﻿using UnityEngine;

public class NetworkDynamicObject : NetworkScript
{
    public DirectionType MoveDirection;
    public Vector2 CurrentPosition;
    public ItemState CurrentState = ItemState.None;
    public GameItemType GameItemType = GameItemType.None;
}

