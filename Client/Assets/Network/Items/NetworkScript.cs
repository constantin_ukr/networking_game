﻿using UnityEngine;

public class NetworkScript : MonoBehaviour {

	public short NetworkId;
	public string NetworkGameObjectName;

	protected virtual void Awake()
	{
		NetworkGameObjectName = gameObject.name;
	}
}