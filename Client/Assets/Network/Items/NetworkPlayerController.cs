﻿using System;
using UnityEngine;

public class NetworkPlayerController : NetworkDynamicObject
{
    private TankController _controller;
    private bool _changedState;
    private bool _fired;

    public bool ChangedState => _changedState;
    public bool Fired => _fired;    

    public void ChangeStateResume()
    {
        _changedState = false;
        _fired = false;
    }

    public void SetLastSendPosition(Vector2 lastPoisition)
    {
        if (!(Mathf.Approximately(CurrentPosition.x, lastPoisition.x) && Mathf.Approximately(CurrentPosition.y, lastPoisition.y)))
        {
            _changedState = true;
        }
    }

    protected override void Awake()
    {
        base.Awake();
        _controller = GetComponent<TankController>();       
        _controller.Fire += OnFire;
        _controller.DirectionAction += OnDirectionAction;
    }

    private void Start()
    {
        CurrentPosition = _controller.transform.position;
        //_previousPosition = CurrentPosition;
        //_prevDirectionName = _controller.LastDirection.DirectionName;
        MoveDirection = (DirectionType)Enum.Parse(typeof(DirectionType), _controller.LastDirection.DirectionName);
    }

    private void LateUpdate()
    {
        //_previousPosition = transform.position;
        //_prevDirectionName = _controller.LastDirection.DirectionName;        
    }

    private void OnDirectionAction(object sender, EventArgs e)
    {
        MoveDirection = (DirectionType)Enum.Parse(typeof(DirectionType), _controller.LastDirection.DirectionName);
        //print($"{_controller.LastDirection.DirectionName} - {MoveDirection}");
        CurrentPosition = _controller.transform.position;
        _changedState = true;
    }

    private void OnFire(object sender, EventArgs e)
    {
        _fired = true;
        _changedState = true;
    }
}