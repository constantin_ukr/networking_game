﻿public enum ItemState : byte
{
    None,
    Initialize,
    Update,
    Destroy
}