﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    private NetworkPlayerController _ownController;
    private List<NetworkDynamicObject> _networkObjects;
    private Wall _wall;
    private Queue<ReceiveData> _receiveData = new Queue<ReceiveData>();

    public TankController TankControllerPrefab;
    public Eagle Eagle;
    public Transform GameRooTransform;
    public DynamicObjectPresenter EnemyPresenter;
    public DynamicObjectPresenter OtherPlayerPresenter;
    public Bullet BulletPrefab;
    public LevelModel Level;
    public List<NetworkDynamicObject> NetworkObjects => _networkObjects;
    
    // MUST BE FIXED Late
    public event EventHandler<NetworkPlayerController> PlayerInited = delegate {};
    public List<int> PrevoiusWorldSnapshotInitData = new List<int>();
    public List<int> PrevoiusWorldSnapshotDeleteData = new List<int>();

    private void Start()
    {
        _wall = FindObjectOfType<Wall>();
        _networkObjects = new List<NetworkDynamicObject>();
    }

    private void FixedUpdate()
    {        
        while (_receiveData.Count > 0)
        { 
            var data = _receiveData.Dequeue();
            
            switch (data.EventType)
            {
                case "createPlayer":
                {
                    var component = (GameEntityComponentData)data.Data;
                    CreatePlayerInstance(component.X, component.Y, component.NetworkId, component.Direction);
                    PlayerInited(this, _ownController);
                    break;
                }
                case "diffMap":
                {
                    var component = (DiffMapComponentData)data.Data;
                    SetDiffMap(component.DiffMap);
                    break;
                }
                case "createEntity":
                {
                    for (int i = 0; i < data.DataArray.Length; i++)
                    { 
                        var component = (GameEntityComponentData)data.DataArray[i];
                        //ConsoleLog.Instance.AddLog($"C Id: {component.NetworkId} Pos: {component.X} {component.Y}");
                        InstantiateGameEntity(component);
                    }
                        
                    break;
                }
                case "updateEntity":
                {
                    for (int i = 0; i < data.DataArray.Length; i++)
                    {
                        var component = (GameEntityComponentData)data.DataArray[i];
                        UpdateGameEntity(component);
                    }
                        
                    break;
                }
                case "deleteEntity":
                {
                    for (int i = 0; i < data.DataArray.Length; i++)
                    {
                        var component = (GameEntityComponentData)data.DataArray[i];
                        //ConsoleLog.Instance.AddLog($"D Id: {component.NetworkId} Pos: {component.X} {component.Y}");
                        DestroyGameEntity(component);
                    }
                    break;
                }
                case "gameOver":
                {                       
                    var component = (GameOverComponentData)data.Data;
                    //ConsoleLog.Instance.AddLog($"D Id: {component.NetworkId} Pos: {component.X} {component.Y}");
                    GameOver(component.Result);                        
                    break;
                }
                default:
                    break; 
            }            
        }
    }

    public void ReceiveData(ReceiveData data)
    {
        if (data == null || string.IsNullOrWhiteSpace(data.EventType) || (data.Data == null && data.DataArray == null))
        {
            return;
        }
        
        _receiveData.Enqueue(data);
    }

    public void PlayerDisconnected(short networkId)
    {
        NetworkDynamicObject playerDisconnected = null;
        for (int i = 0; i < _networkObjects.Count; i++)
        {
            if (_networkObjects[i].NetworkId == networkId)
            {
                playerDisconnected = _networkObjects[i];
                _networkObjects.Remove(_networkObjects[i]);
                break;
            }
        }

        if (playerDisconnected != null)
        {
            playerDisconnected.GetComponent<SpriteRenderer>().color = Color.red;
            Destroy(playerDisconnected);
            Destroy(playerDisconnected.gameObject, 1f);
        }
    }

    private void UpdateGameEntity(GameEntityComponentData entity)
    {
        var data = _networkObjects.ToArray();
        for (var i = 0; i < data.Length; i++)
        {
            if (data[i].NetworkId == entity.NetworkId)
            {
                data[i].CurrentPosition = new Vector2(entity.X, entity.Y);
                data[i].CurrentState = entity.CurrentState;
                data[i].MoveDirection = entity.Direction;
                //ConsoleLog.Instance.AddLog($"UPD Id: {update[i].NetworkId} MD: {update[i].MoveDirection} Pos: {update[i].CurrentPosition}"); 
                break;
            }
        }
    }

    private void DestroyGameEntity(GameEntityComponentData entity)
    {
        var id = entity.NetworkId;
        if (_ownController.NetworkId == entity.NetworkId)
        {
            _ownController.GetComponent<TankController>().ExplodingAnimator.enabled = true;
            Destroy(_ownController);
            return;
        }

        var data = _networkObjects.ToArray();
        for (var i = 0; i < data.Length; i++)
        {
            if (data[i].NetworkId == id)
            {
                data[i].CurrentPosition = new Vector2(entity.X, entity.Y);
                data[i].CurrentState = entity.CurrentState;
                data[i].MoveDirection = entity.Direction;
                break;
            }

            if (data[i].NetworkId == Eagle.NetworkId)
            {
                Eagle.SetDeadSprite();
            }
        }
    }

    private void SetDiffMap(Tuple<sbyte, sbyte>[] diffMap)
    {
        //ConsoleLog.Instance.AddLog($"SetMapDiff count: {diff.Count}");
        for (var i = 0; i < diffMap.Length; i++)
        {
            _wall.Tilemap.SetTile(new Vector3Int(diffMap[i].Item1, diffMap[i].Item2, 0), null);
        }
    }

    private void CreatePlayerInstance(float x, float y, short playerId, DirectionType direction)
    {
        if(_ownController != null)
        {
            throw new Exception("WTF, _ownController exist");
        }

        var  tankController = Instantiate(TankControllerPrefab, GameRooTransform);
        tankController.enabled = false;
        var controller = tankController.GetComponent<NetworkPlayerController>();
        if (controller == null)
        {
            controller = tankController.gameObject.AddComponent<NetworkPlayerController>();
        }
        tankController.transform.position = new Vector3(x, y, 0);
        _ownController = controller;
        _ownController.NetworkId = playerId;
        tankController.enabled = true;
    }

    private void InstantiateGameEntity(GameEntityComponentData entity)
    {
        var position = new Vector3(entity.X, entity.Y, 0);
        switch (entity.GameItemType)
        {
            case GameItemType.PlayerController:                    
                //ConsoleLog.Instance.AddLog($"INIT OP Id: {init[i].NetworkId} MD: {init[i].MoveDirection} Pos: {init[i].CurrentPosition}");
                AddOtherPlayer(position, entity.NetworkId, entity.Direction);
                break;
            case GameItemType.Bullet:
                //ConsoleLog.Instance.AddLog($"INIT BL Id: {init[i].NetworkId} MD: {init[i].MoveDirection} Pos: {init[i].CurrentPosition}");
                AddBullet(position, entity.NetworkId, entity.Direction);
                break;
            case GameItemType.EnemyLevelOne:
                //ConsoleLog.Instance.AddLog($"INIT EN Id: {init[i].NetworkId} MD: {init[i].MoveDirection} Pos: {init[i].CurrentPosition}");
                CreateEnemy(position, entity.NetworkId, entity.Direction);
                break;
            //case GameItemType.EnemyLevelTwo:
            //    CreateEnemy(init[i].CurrentPosition, init[i].NetworkId, (DirectionType)init[i].MoveDirection);
            //    break;
            case GameItemType.None:
            default:
                break;
        }
    }

    private void AddOtherPlayer(Vector3 spawnPosition, short networkId, DirectionType direction)
    {
        if(_ownController.NetworkId == networkId)
        {
            return;
        }

        var tankController = Instantiate(OtherPlayerPresenter, transform);
        tankController.enabled = false;
        var controller = tankController.gameObject.AddComponent<NetworkDynamicObject>();
        controller.NetworkId = networkId;
        controller.NetworkGameObjectName = tankController.name;
        controller.CurrentPosition = spawnPosition;
        controller.MoveDirection = direction;
        tankController.Init(null);
        tankController.transform.position = spawnPosition;
        tankController.enabled = true;
        _networkObjects.Add(controller);
    }

    private void AddBullet(Vector3 spawnPosition, short networkId, DirectionType direction)
    {
        var bullet = Instantiate(BulletPrefab, transform.parent); // verify local and world position!!!
        bullet.enabled = false;
        var controller = bullet.gameObject.AddComponent<NetworkDynamicObject>();
        controller.NetworkId = networkId;
        controller.NetworkGameObjectName = bullet.name;
        controller.CurrentPosition = spawnPosition;        
        var zRotation = GetZRotationByDirection(direction);
        var size = GetBulletColliderSizeByDirection(direction);
        bullet.Fire(spawnPosition, zRotation, size);
        bullet.enabled = true;
        _networkObjects.Add(controller);
    }

    private void CreateEnemy(Vector3 spawnPosition, short networkId, DirectionType direction)
    {
        var enemy = Instantiate(EnemyPresenter, transform);
        enemy.enabled = false;
        var networkDynamicObject = enemy.gameObject.AddComponent<NetworkDynamicObject>();
        networkDynamicObject.NetworkId = networkId;
        var enemyModel = Level.Enemies[0];
        enemy.Init(enemyModel);
        enemy.transform.position = spawnPosition;
        enemy.LastDirection = enemy.Directions[(byte)direction];
        enemy.enabled = true;
        _networkObjects.Add(networkDynamicObject);
    }

    private float GetZRotationByDirection(DirectionType direction)
    {
        switch (direction)
        {
            case DirectionType.Left:
                return -90f;
            case DirectionType.Up:
                return 180f;
            case DirectionType.Right:
                return 90f;
            case DirectionType.Down:
            default:
                return 0;
        }
    }

    private Vector2 GetBulletColliderSizeByDirection(DirectionType direction)
    {
        switch (direction)
        {
            case DirectionType.Left:                         
            case DirectionType.Right:
                return new Vector2(0.5f, 0.17f);
            case DirectionType.Up:                
            case DirectionType.Down:
                return new Vector2(0.7f, 0.17f);
            default:
                throw new ArgumentException("How come?");
        }
    }

    private void GameOver(byte result)
    {
        GameRooTransform.gameObject.SetActive(false);
        var canvas = FindObjectOfType<Canvas>();
        var gameOverText = canvas.gameObject.GetComponentsInChildren<Text>(true).First(o => o.name == "GameOverText");
        if (gameOverText != null)
        {
            string resultText = result == 0 ? "lose" : "win";
            gameOverText.text = "GAME OVER!" + Environment.NewLine + "You " + resultText;
            gameOverText.enabled = true;
            gameOverText.gameObject.SetActive(true);
        }
        
    }
}