﻿public enum DirectionType : byte
{
    Left = 0,
    Up = 1,
    Right = 2,
    Down = 3 
}