public class ReceiveData
{
    public string EventType;
    public IComponentData Data;
    public IComponentData[] DataArray;
}