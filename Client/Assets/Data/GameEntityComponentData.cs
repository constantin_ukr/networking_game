public struct GameEntityComponentData : IComponentData
{
    public float X;
    public float Y;
    public short NetworkId;
    public DirectionType Direction;
    public ItemState CurrentState;
    public GameItemType GameItemType;
}