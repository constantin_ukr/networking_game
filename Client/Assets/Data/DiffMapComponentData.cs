﻿using System;

public struct DiffMapComponentData : IComponentData
{
    public Tuple<sbyte, sbyte>[] DiffMap;
}

