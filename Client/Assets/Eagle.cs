﻿using System;
using UnityEngine;

public class Eagle : MonoBehaviour
{
    private short _networkId;
    public SpriteRenderer View;
    public Sprite DieView;

    public short NetworkId => _networkId;
    
    private void Awake()
    {
        var networkScript = GetComponent<NetworkScript>();
        if (networkScript == null)
        {
            networkScript = gameObject.AddComponent<NetworkScript>();
        }

        _networkId = networkScript.NetworkId;
    }

    public void SetDeadSprite()
    {
        View.sprite = DieView;
    }
}
