﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    private NetworkDynamicObject _networkDynamicObject;
    private bool _inited;
    public SpriteRenderer View;
    public Animator ExplosionAnimation;    
    public BoxCollider2D BoxCollider2D;

    public void Fire(Vector2 position, float zRotation, Vector2 colliderSize)
    {
        View.transform.eulerAngles = new Vector3(0, 0, zRotation);
        transform.position = position;
        BoxCollider2D.size = colliderSize;
    }

    void Update ()
    {
        if (!_inited)
        {
            _networkDynamicObject = GetComponent<NetworkDynamicObject>();
            _inited = _networkDynamicObject != null;
            return;
        }
        
        transform.position = Vector2.Lerp(transform.position, _networkDynamicObject.CurrentPosition, 0.15f);
        if (_networkDynamicObject.CurrentState == ItemState.Destroy)
        {
            ExplosionAnimation.enabled = true;
            _inited = false;
        }
    }

    public void FinishExplosionAnimation()
    {
        Destroy(gameObject);
    }
}
