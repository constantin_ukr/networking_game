﻿using System.Collections.Generic;
using UnityEngine;

public class DynamicObjectPresenter : MonoBehaviour
{
    private NetworkDynamicObject _networkDynamicObject;
    private bool _inited;
    public SpriteRenderer View;
    public List<DirectionParams> Directions;
    public DirectionParams LastDirection;
    public Animator ExplodingAnimator;

    public void Init(object model)
    {
        var data = model as EnemyModel;
        if (data != null)
        {
            View.sprite = data.Views.Find(_ => _.Name == "Up").Sprite;

            foreach (var directionParams in Directions)
            {
                directionParams.Sprite = data.Views.Find(_ => _.Name == directionParams.DirectionName).Sprite;
            }
        }

        _networkDynamicObject = GetComponent<NetworkDynamicObject>();
        _inited = _networkDynamicObject != null;
    }

    void Update()
    {
        if (!_inited)
        {
            _networkDynamicObject = GetComponent<NetworkDynamicObject>();
            _inited = _networkDynamicObject != null;
            return;
        }
        
        LastDirection = Directions[(byte)_networkDynamicObject.MoveDirection];
        if (View.sprite != LastDirection.Sprite)
        {
            View.sprite = LastDirection.Sprite;
        }
        transform.position = Vector2.Lerp(transform.position, _networkDynamicObject.CurrentPosition, 0.15f);
        
        if (_networkDynamicObject.CurrentState == ItemState.Destroy)
        {
            ExplodingAnimator.enabled = true;
            _inited = false;
        }
    }
    
    public void FinishExplosionAnimation()
    {
        Destroy(gameObject);
    }
}